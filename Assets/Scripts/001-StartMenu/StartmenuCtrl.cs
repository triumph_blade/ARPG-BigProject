﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartmenuCtrl : MonoBehaviour {

    public static StartmenuCtrl instance;

    [Header("All Panel")]
    public GameObject beginPanel;
    public GameObject loginPanel;
    public GameObject registerPanel;
    public GameObject serverPanel;
    public GameObject characterConfirmPanel;
    public GameObject characterSelectPanel;

    [Header("Begin")]
    public Text usernameTextBegin;
    public Text serverTextBegin;

    [Header("Login")]
    public InputField usernameInputLogin;
    public InputField passwordInputLogin;

    [Header("Register")]
    public InputField usernameInputRegister;
    public InputField passwordInputRegister;
    public InputField repasswordInputRegister;
    
    [Header("Server")]
    public GameObject serverItemRed;
    public GameObject serverItemGreen;
    public Transform serverListContent;
    public GameObject serverSelectedGo;


    [Header("Character Confirm")]
    public Text characterNameText;
    public Text characterLevelText;
    public Transform[] characterModelArray;
    public GameObject[] characterSelectedResArray;

    [Header("Character Select")]
    public InputField characterNameInput;
    public GameObject[] characterPicArray;

    public static string username = "请登录";
    public static string password;



    private List<Quaternion> charOriginRotationList=new List<Quaternion>();
    private GameObject characterSelect;

    private bool isInitServerList = false;
    private List<GameObject> serverGoList = new List<GameObject>();
    private float listVerticalStep;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        beginPanel.SetActive(true);
        InitServerList();
        for (int i = 0; i < characterModelArray.Length; i++)
            charOriginRotationList.Add(characterModelArray[i].rotation);
    }

    public void OnUsernameClick()
    {
        //进入账户登陆界面
        SwitchBetweenTwoPanel(beginPanel, loginPanel);
    }

    public void OnServerClick()
    {
        //进入选择服务器界面
        SwitchBetweenTwoPanel(beginPanel, serverPanel);
    }

    public void OnEnterGameClick()
    {
        //1.连接服务器，验证用户名和服务器
        //TODO

        //2.进入角色选择界面
        //TODO
        SwitchBetweenTwoPanel(beginPanel, characterConfirmPanel);
    }

    public void OnEnterCharacterSelectClick()
    {
        SwitchBetweenTwoPanel(characterConfirmPanel, characterSelectPanel);
    }

    public void OnLoginClick()
    {
        //得到用户名与密码，然后存储
        if (usernameInputLogin.text != "")
            username = usernameInputLogin.text;
        if (passwordInputLogin.text != "")
            password = passwordInputLogin.text;

        usernameTextBegin.text = username;

        OnLoginCloseClick();
       
    }

    public void OnEnterRegisterClick()
    {
        //隐藏注册面板显示登陆面板
        SwitchBetweenTwoPanel(loginPanel, registerPanel);
    }

    public void OnLoginCloseClick()
    {
        SwitchBetweenTwoPanel(loginPanel, beginPanel);
        //清空密码框
        passwordInputLogin.text = "";
    }

    public void OnRegisterCancelClick()
    {
        SwitchBetweenTwoPanel(registerPanel, loginPanel);
        //清空输入框
        usernameInputRegister.text = "";
        passwordInputRegister.text = "";
        repasswordInputRegister.text = "";
    }

    public void OnRegisterCloseClick()
    {
        OnRegisterCancelClick();
    }

    /// <summary>
    /// 
    /// </summary>
    public void OnServerSelectedClick()
    {
        SwitchBetweenTwoPanel(serverPanel, beginPanel);
        serverTextBegin.text = serverSelectedGo.GetComponentInChildren<Text>().text;
    }

    /// <summary>
    /// 注册并登陆按钮点击
    /// </summary>
    public void OnRegistAndLoginClick()
    {
        //1.本地校验，连接服务器进行验证
        //TODO
        //2.连接失败
        //TODO
        //3.连接成功，保存用户名与密码
        if (usernameInputRegister.text != "")
            username = usernameInputRegister.text;
        if (passwordInputRegister.text != "")
            password = passwordInputRegister.text;
        

        usernameTextBegin.text = username;

        OnRegisterCancelClick();
    }


    public void OnCharSelectBackClick()
    {
        SwitchBetweenTwoPanel(characterSelectPanel, characterConfirmPanel);
        //清空姓名输入框
        characterNameInput.text = "";
    }

    /// <summary>
    /// 确认姓名按钮点击
    /// </summary>
    public void OnCharSelectNameSureClick()
    {
        //1.判断姓名输入是否正确
        //TODO
        //2.判断角色是否选中
        //TODO
        int index = -1;
        for(int i=0;i<characterPicArray.Length;i++)
        {
            if(characterSelect==characterPicArray[i])
            {
                index = i;
                break;
            }
        }
        if (index == -1) return;
        foreach (GameObject characterSelectedRes in characterSelectedResArray)
            characterSelectedRes.SetActive(false);
        characterSelectedResArray[index].SetActive(true);

        characterNameText.text = characterNameInput.text;
        characterLevelText.text = "Lv.1";
        OnCharSelectBackClick();


    }

    public void OnCharConfirmBackClick()
    {
        SwitchBetweenTwoPanel(characterConfirmPanel, beginPanel);
    }


    /// <summary>
    /// 处理两个界面的跳转
    /// </summary>
    /// <param name="from">起点界面</param>
    /// <param name="to">目标界面</param>
    private void SwitchBetweenTwoPanel(GameObject from , GameObject to)
    {
        for (int i = 0; i < characterModelArray.Length; i++)
            characterModelArray[i].rotation = charOriginRotationList[i] ;
        from.SetActive(false);
        to.SetActive(true);
    }

    /// <summary>
    /// 初始化服务器列表
    /// </summary>
    public void InitServerList()
    {
        if (isInitServerList) return;
        //1.连接服务器，取得游戏服务器列表信息
        //TODO
        //2.根据上面的信息，添加服务器列表
        for(int i = 0; i < 20; i++)
        {
            string ip = "127.0.0.1:9080";
            string serverName = (i + 1) + "区 马达加斯加";
            int count = Random.Range(0, 100);
            GameObject go = null;
            if (count > 50)
            {
                //火爆
                go = GameObject.Instantiate(serverItemRed, serverItemRed.transform); 
            }
            else
            { 
                //流畅
                go = GameObject.Instantiate(serverItemGreen, serverItemGreen.transform);
            }
            if (go == null) continue;
            go.transform.SetParent(serverListContent);
            go.transform.localScale = Vector3.one;
            ServerProperty sp = go.GetComponent<ServerProperty>();
            sp.ip = ip;
            sp.serverName = serverName;
            sp.count = count;
        }
        isInitServerList = true;
    }


    /// <summary>
    /// 改变"已选择服务器"为已选项
    /// </summary>
    /// <param name="serverGo">已选中的服务器按钮</param>
    public void ChangeServerSelected(GameObject serverGo)
    {
        serverSelectedGo.GetComponent<Image>().sprite = serverGo.GetComponent<Image>().sprite;
        serverSelectedGo.GetComponentInChildren<Text>().text = serverGo.GetComponentInChildren<Text>().text;
        serverSelectedGo.GetComponentInChildren<Text>().color = serverGo.GetComponentInChildren<Text>().color;
    }

    /// <summary>
    /// 显示已选中的角色
    /// </summary>
    /// <param name="charGo">已选中的角色</param>
    public void ChangeCharSelected(GameObject charGo)
    {
        iTween.ScaleTo(charGo, new Vector3(1.4f, 1.4f, 1f), 0.5f);
        if (characterSelect != null)
        {
            iTween.ScaleTo(characterSelect, new Vector3(1f, 1f, 1f), 0.5f);
        }
        if (charGo == characterSelect)
        {
            iTween.ScaleTo(characterSelect, new Vector3(1f, 1f, 1f), 0.5f);
            characterSelect = null;
        }
       else
        {
            characterSelect = charGo;
            //print(characterSelect.name);
        }
            
        
    }

}
