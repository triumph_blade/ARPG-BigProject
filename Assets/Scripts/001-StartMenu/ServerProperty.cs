﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 每个服务器按钮的属性
/// </summary>
public class ServerProperty : MonoBehaviour {

    public string ip = "127.0.0.1:9080";
    public string serverName
    {
        set
        {
            transform.Find("Text").GetComponent<Text>().text = value;
        }
        get
        {
            return serverName;
        }
    }
    public  int count=100;  //在线人数



    public void OnThisClick()
    {
         //transform.root.SendMessage("ChangeServerSelected", this.gameObject);
         //更换已选服务器的显示
        StartmenuCtrl.instance.ChangeServerSelected(this.gameObject);
    }


}
