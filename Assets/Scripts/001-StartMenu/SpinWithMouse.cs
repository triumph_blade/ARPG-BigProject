﻿
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class SpinWithMouse : MonoBehaviour,IPointerDownHandler,IPointerUpHandler
{
    public GameObject obj;
    private float m_fDeltaX;
    private float m_fDeltaY;
    public float m_fSpeed = 1000;
    private bool isPress = false;
   // private Animation animation;

    void Start()
    {
        //animation = obj.transform.Find("stand").GetComponent<Animation>();
        //print(animation);
        //animation.Play("stand");
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isPress = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isPress = false;
    }

    private void Update()
    {
        if (isPress)
        {
            m_fDeltaX = Input.GetAxis("Mouse X") * Time.deltaTime * m_fSpeed;
            m_fDeltaY = Input.GetAxis("Mouse Y") * Time.deltaTime * m_fSpeed;
            obj.transform.Rotate(new Vector3(0, -m_fDeltaX, 0), Space.World);
        }
        else 
        {
            obj.transform.Rotate(-Vector3.up * 0.5f, Space.World);
        }
    }
    
    public void OnThisClick()
    {
        StartmenuCtrl.instance.ChangeCharSelected(this.gameObject);
    }

}
