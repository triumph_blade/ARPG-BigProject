﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum AttackRange
{
    Forward,
    Around
}



public class PlayerAttack : MonoBehaviour {

    private Animator anim;
    private Dictionary<string, PlayerEffect> effectDict = new Dictionary<string, PlayerEffect>();
    private new  Rigidbody rigidbody;
    private float timer=0;
    private float timeLimit=0.3f;
    private bool isDash=false;
    private CapsuleCollider cc;
    private float dashSpeed;
    public float distanceAttackForward = 2f;
    public float distanceAttackAround = 2f;
    public List<PlayerEffect> peArrList=new List<PlayerEffect>();

    public int[] damageArr = new int[] { 20, 30, 30, 30 };

    private void Start()
    {
        anim = GetComponent<Animator>();
        peArrList.AddRange(GetComponentsInChildren<PlayerEffect>());
        foreach(PlayerEffect pe in peArrList)
        {
            effectDict.Add(pe.gameObject.name, pe);
        }
        rigidbody = GetComponent<Rigidbody>();
        cc = GetComponent<CapsuleCollider>();
    }

    public void OnAttackButtonClick(PosType posType,bool isPress)
    {
        if (posType == PosType.Basic)
        {
            if(isPress)
            anim.SetTrigger("Attack");
        }
        else
        {
            anim.SetBool("Skill" +(int)posType,isPress);
        }
    }


    /// <summary>
    ///Event function 
    /// </summary>
    /// <param name="args">
    /// 0 normal skill1 skill2 skill3
    /// 1 effect name 
    /// 2 sound name
    /// 3 dash speed
    /// 4 strike height
    /// </param>
    void Attack(string args)
    {
        string[] proArr = args.Split(',');

        //Show Atk Effect
        string effectName = proArr[1];
        PlayerEffect pe;
        if (effectDict.TryGetValue(effectName, out pe))
            pe.Show();

        //Player sound
        string soundName = proArr[2];
        SoundManager.instance.Play(soundName);

        //dash forward
         dashSpeed = float.Parse(proArr[3]);
        if (dashSpeed > 0.1f)
        {
            isDash = true;
            //RaycastHit hit;
            //bool isHit = Physics.Raycast(transform.position, transform.forward, out hit, cc.radius*transform.localScale.x, LayerMask.GetMask("Wall"));
            //if (isHit)
            //{
            //    if (Vector3.Distance(transform.position, hit.point) > cc.radius * transform.localScale.x)
            //    {
            //        iTween.MoveBy(gameObject, Vector3.forward*dashSpeed, 0.3f);
            //    }
            //    else
            //    {
            //        iTween.MoveBy(gameObject, Vector3.back*dashSpeed , 0.3f);
            //    }
            //}
            //else
            //{
            //    iTween.MoveBy(gameObject, Vector3.forward * dashSpeed, 0.3f);
            //}
        }

        string posType = proArr[0];
        if(posType == "normal")
        {
            ArrayList arrList = GetEnemyInAttackRange(AttackRange.Forward);
            foreach (GameObject go in arrList)
            {
                go.SendMessage("TakeDamage",damageArr[0]+","+proArr[3]+","+proArr[4]); //TODO:参数尚未知
            }
        }
        else if(posType == "skill1")
        {
            ArrayList arrList = GetEnemyInAttackRange(AttackRange.Around);
            foreach(GameObject go in arrList)
                go.SendMessage("TakeDamage", damageArr[0] + "," + proArr[3] + "," + proArr[4]); //TODO:参数尚未知
        }
        

    }


    private void Update()
    {
        if (isDash)
        {
            timer += Time.deltaTime;
            rigidbody.velocity = transform.forward * dashSpeed;
            if (timer >= timeLimit)
            {
                rigidbody.velocity = Vector3.zero;
                isDash = false;
                timer = 0;
                return;
            }
        }
        else return;
    }

    /// <summary>
    /// 得到在攻击范围之内的敌人
    /// </summary>
    /// <returns></returns>
    ArrayList GetEnemyInAttackRange(AttackRange attackRange)
    {
        ArrayList arrList = new ArrayList();
        if (attackRange == AttackRange.Forward)
        {
            foreach(GameObject go in TranscriptManager.instance.enemyList)
            {
                //将敌人的世界坐标转化到玩家的局部坐标系中
                Vector3 pos = transform.InverseTransformPoint(go.transform.position);
                if (pos.z > -0.5)
                {
                    float distance = Vector3.Distance(Vector3.zero, pos);
                    if (distance < distanceAttackForward)
                        arrList.Add(go);
                }
            }
        }
        else
        {
            foreach (GameObject go in TranscriptManager.instance.enemyList)
            {
                float distance = Vector3.Distance(transform.position,go.transform.position);
                if (distance < distanceAttackAround)
                    arrList.Add(go);
            }
        }
        return arrList;
    }



    void ShowEffectDevilHand()
    {
        string effectName = "DevilHandMobile";
        PlayerEffect pe;
        effectDict.TryGetValue(effectName, out pe);
        ArrayList array = GetEnemyInAttackRange(AttackRange.Forward);
        foreach(GameObject go in array)
        {
            RaycastHit hit;
            bool isHit =Physics.Raycast(go.transform.position + Vector3.up, Vector3.down, out hit, 10f, LayerMask.GetMask("Ground"));
            if(isHit)
            {
                GameObject.Instantiate(pe, hit.point, Quaternion.identity);
            }
        }
    }

    void ShowEffectCrowStorm()
    {
        string effectName = "CrowstormEffect";
        PlayerEffect pe;
        effectDict.TryGetValue(effectName, out pe);
        GameObject.Instantiate(pe, gameObject.transform.position, Quaternion.identity);
    }

}
