﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public GameObject damageEffectPfb;
    private Animation animation;
    public int hp = 200;
	// Use this for initialization
	void Start () {
        animation = GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    //收到多少伤害，浮空与后退距离
    void TakeDamage(string args)
    {
        if (hp <= 0) return;
        string[] proArr = args.Split(',');
        //减去伤害值
        int damage = int.Parse(proArr[0]);
        hp -= damage;
        //受击动画
        animation.Play("takedamage");
        StartCoroutine(ResetAnimation(animation.GetClip("takedamage").length));
        //浮空与击退
        float backDistance = float.Parse(proArr[1]);
        float strikeHeight = float.Parse(proArr[2]);
        iTween.MoveBy(gameObject, 
            transform.InverseTransformDirection(TranscriptManager.instance.Player.transform.forward) * backDistance*0.5f
            + Vector3.up * strikeHeight,
            0.3f);
        //出血特效
        GameObject.Instantiate(damageEffectPfb, transform.position+Vector3.up+Vector3.forward, Quaternion.identity);
        if (hp <= 0) Dead();
       
    }

    IEnumerator ResetAnimation(float timeAfter)
    {
        yield return new WaitForSeconds(timeAfter);
        animation.Play("idle");
    }

    void Dead()
    {

    }
}
