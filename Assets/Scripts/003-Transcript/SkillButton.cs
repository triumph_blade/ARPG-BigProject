﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SkillButton : MonoBehaviour,IPointerDownHandler ,IPointerUpHandler{

    public PosType posType = PosType.Basic;
    public float coldTime = 4;
    private float coldTimer; //剩余的冷却时间
    private Transform mask;
    private Image maskImage;
    private PlayerAttack playerAttack;
    private Button btn;
    private bool isPress=false;
    void Start()
    {
        playerAttack = TranscriptManager.instance.Player.GetComponent<PlayerAttack>();
        mask = transform.Find("Mask");
        if(mask!=null)
            maskImage = mask.GetComponent<Image>();
        btn = GetComponent<Button>();
    }


    void Update()
    {
        if (maskImage == null) return;
        if (coldTimer > 0)
        {
            coldTimer -= Time.deltaTime;
            maskImage.fillAmount = coldTimer / coldTime;
        }
        else
        {
            maskImage.fillAmount = 0;
            btn.interactable = true;
        }
    }


    

    public void OnPointerDown(PointerEventData eventData)
    {
        isPress = true;
        if (btn.interactable == true)
        {
            playerAttack.OnAttackButtonClick(posType, isPress);
            if (maskImage != null)
            {
                coldTimer = coldTime;
                btn.interactable = false;
            }
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isPress = false;
        playerAttack.OnAttackButtonClick(posType, isPress);
    }
}
