﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 管理特效的播放
/// </summary>
public class PlayerEffect : MonoBehaviour {
    //针对红色刀光
    private Renderer[] rendererArr;
    private NcCurveAnimation[] curveAnimArr;
    //针对白色细刀光
    private GameObject effectOffset;

	// Use this for initialization
	void Start () {
        rendererArr = GetComponentsInChildren<Renderer>();
        curveAnimArr = GetComponentsInChildren<NcCurveAnimation>();
        if (transform.Find("EffectOffset")!=null)
        effectOffset = transform.Find("EffectOffset").gameObject;
    }
	
	// Update is called once per frame
	void Update () {
        //for test
        //if (Input.GetMouseButtonDown(0))
        //    Show();
	}

    public void Show()
    {
        if (effectOffset != null)
        {
            effectOffset.SetActive(false);
            effectOffset.SetActive(true);
        }
        else
        {
            foreach (Renderer renderer in rendererArr)
            {
                renderer.enabled = true;
            }
            foreach (NcCurveAnimation curveAnim in curveAnimArr)
            {
                curveAnim.ResetAnimation();
            }
        }
    
    }


}
