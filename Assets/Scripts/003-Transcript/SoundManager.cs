﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public static SoundManager instance;
    public AudioClip[] audioClipArr;
    public Dictionary<string, AudioClip> audioDict = new Dictionary<string, AudioClip>();
    private  AudioSource audioSource;
    public bool isQuiet = false;

    private void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {
		foreach(AudioClip ac in audioClipArr)
        {
            audioDict.Add(ac.name, ac);
        }
        audioSource = GetComponent<AudioSource>();
	}
	
    public void Play(string audioName)
    {
        if (isQuiet) return;
        AudioClip ac;
        if (audioDict.TryGetValue(audioName, out ac))
            this.audioSource.PlayOneShot(ac);
    }

    public void Play(string audioName,AudioSource audioSource)
    {
        if (isQuiet) return;
        AudioClip ac;
        if (audioDict.TryGetValue(audioName, out ac))
            audioSource.PlayOneShot(ac);
    }







	// Update is called once per frame
	void Update () {
		
	}
}
