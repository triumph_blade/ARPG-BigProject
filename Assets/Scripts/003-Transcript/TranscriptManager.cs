﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranscriptManager : MonoBehaviour {

    public static TranscriptManager instance;
    private GameObject player;
    public List<GameObject> enemyList=new List<GameObject>();

    public GameObject Player
    {
        get
        {
            return player;
        }
    }
    private void Awake()
    {
        instance = this;
        player = GameObject.FindWithTag("Player");
    }

}
