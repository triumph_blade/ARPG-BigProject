﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerAutoMove : MonoBehaviour {

    private NavMeshAgent agent;
    private bool isComplete=false;
    public bool IsComplete { get { return isComplete; } set { isComplete = value; } }
    public float minDis = 1f;
    public Transform target;
	// Use this for initialization
	void Start () {
        agent = this.GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
        if (agent.enabled)
        {
            if (agent.remainingDistance < minDis&&agent.remainingDistance!=0)
            {
                agent.Stop();
                agent.enabled = false;
                TaskManager.instance.OnArriveDestination();
            }
        }

        //if (Input.GetMouseButton(0))
        //{
        //    SetDestination(target.position);
        //}

	}

    public void SetDestination(Vector3 targetPos)
    {
        agent.enabled = true;
        agent.SetDestination(targetPos);
    }
}
