﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMove : MonoBehaviour {

    public float velocity=5;
    private new Rigidbody rigidbody;
    private NavMeshAgent agent;
    private Animator animator;
    public bool isTranscript;


	// Use this for initialization
	void Start () {
        animator = gameObject.GetComponent<Animator>();
        rigidbody = gameObject.GetComponent<Rigidbody>();
        agent = gameObject.GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
        WindowsCtrl();
	}

    void WindowsCtrl()
    {
        float h, v;
        Vector3 vel = rigidbody.velocity;
        if (!isTranscript)
        {
          h = Input.GetAxis("Horizontal");
          v = Input.GetAxis("Vertical");
            if ((Mathf.Abs(h) > 0.05f || Mathf.Abs(v) > 0.05f) )
            {
                //rigidbody.isKinematic = false;
                rigidbody.velocity = new Vector3(-h * velocity, vel.y, -v * velocity);
                agent.enabled = false;
                transform.rotation = Quaternion.LookRotation(new Vector3(-h, 0, -v));
            }
            else
            {
                rigidbody.velocity = new Vector3(0, vel.y, 0);
                //rigidbody.isKinematic = true;
            }
        }
        else
        {
            h = -Input.GetAxis("Horizontal");
            v = -Input.GetAxis("Vertical");
            if ((Mathf.Abs(h) > 0.05f || Mathf.Abs(v) > 0.05f) && animator.GetCurrentAnimatorStateInfo(1).IsName("Empty State"))
            {
                //rigidbody.isKinematic = false;
                rigidbody.velocity = new Vector3(-h * velocity, vel.y, -v * velocity);
                agent.enabled = false;
                transform.rotation = Quaternion.LookRotation(new Vector3(-h, 0, -v));
            }
            else
            {
                rigidbody.velocity = new Vector3(0, vel.y, 0);
                //rigidbody.isKinematic = true;
            }
        }
  



        if(Input.GetKey("a") || Input.GetKey("s") || Input.GetKey("w") || Input.GetKey("d") || Input.GetKey("s")
            || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.DownArrow) 
            || Input.GetKey(KeyCode.UpArrow) || agent.enabled == true)
            animator.SetBool("Move", true);
        else 
            animator.SetBool("Move", false);
    }


    void AndroidCtrl()
    {

    }
}
