﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour {
    public Vector3 offset;
    private Transform player;
    private bool isTranscript;
    private void Start()
    {
        player = GameObject.FindWithTag("Player").GetComponent < Transform>();
        isTranscript = player.GetComponent<PlayerMove>().isTranscript;
        if (isTranscript)
            player = player.Find("Bip01");
    }
    private void Update()
    {
        transform.position = player.position + offset;
    }
}
