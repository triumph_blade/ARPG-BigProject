﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskManager : MonoBehaviour {

    public static TaskManager instance;
    private TextAsset taskInfo;
    private ArrayList taskList=new ArrayList();

    public ArrayList TaskList  { get { return taskList; }  }

    private PlayerAutoMove playerAutoMove;
    private PlayerAutoMove PlayerAutoMove
    {
        get
        {
            if (playerAutoMove == null)
                playerAutoMove = GameObject.FindWithTag("Player").GetComponent<PlayerAutoMove>();
            return playerAutoMove;
        }
    }

    private Task currentTask;

    private void Awake()
    {
        instance = this;
        taskInfo = Resources.Load<TextAsset>("InfoText/TaskListInfo");
        InitTask();
    }




    /// <summary>
    /// 初始化任务信息
    /// </summary>
    public void InitTask()
    {
        string[] taskStrArr =  taskInfo.ToString().Split("\r\n".ToCharArray());
        foreach (string str in taskStrArr)
        {
            if (str == "") continue;
            string[] proArray = str.Split('|');
            Task task = new Task();
            task.Id = int.Parse(proArray[0]);
            switch (proArray[1])
            {
                case "Main":
                    task.TaskType = TaskType.Main;
                    break;
                case "Reward":
                    task.TaskType = TaskType.Reward;
                    break;
                case "Daily":
                    task.TaskType = TaskType.Daily;
                    break;
            }
            task.Name = proArray[2];
            task.Icon = proArray[3];
            task.Des = proArray[4];
            task.Coin = int.Parse(proArray[5]);
            task.Diamond = int.Parse(proArray[6]);
            task.NpcTalk = proArray[7];
            task.NpcId = int.Parse(proArray[8]);
            task.TranscriptId = int.Parse(proArray[9]);
            task.NpcIcon = proArray[10];
            TaskList.Add(task);
        }
    }

    //执行某个任务
    public void OnExcuteTask(Task task)
    {
        currentTask = task;
        if (task.TaskProgress == TaskProgress.Unstart)//导航到Npc接受任务
            PlayerAutoMove.SetDestination(NPCManager.instance.GetNpcById(task.Id).transform.position);
        if (task.TaskProgress == TaskProgress.Accept)
            playerAutoMove.SetDestination(NPCManager.instance.transcript.transform.position);
    
    }

    public void OnAcceptTask()
    {
        currentTask.TaskProgress = TaskProgress.Accept;
        playerAutoMove.SetDestination(NPCManager.instance.transcript.transform.position);
    }

    public void OnArriveDestination()
    {
        object obj = new object();
        obj = currentTask;
        GameObject.FindWithTag("UI").SendMessage("OnArriveDestination",obj);
    }

}
