﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TaskType
{
    Main,
    Reward,
    Daily
}

public  enum TaskProgress
{
    Unstart,
    Accept,
    Complete,
    Reward
}

public class Task {
    private int id;
    private TaskType taskType;
    private string name;
    private string icon;
    private string des;
    private int coin;
    private int diamond;
    private string npcTalk;
    private int npcId;
    private int transcriptId;
    private string npcIcon;
    private TaskProgress taskProgress = TaskProgress.Unstart;

    public delegate void OnTaskChangeEvent();
    public event OnTaskChangeEvent OnTaskChange;

    public int Id
    {
        get
        {
            return id;
        }

        set
        {
            id = value;
        }
    }

    public TaskType TaskType
    {
        get
        {
            return taskType;
        }

        set
        {
            taskType = value;
        }
    }

    public string Name
    {
        get
        {
            return name;
        }

        set
        {
            name = value;
        }
    }

    public string Des
    {
        get
        {
            return des;
        }

        set
        {
            des = value;
        }
    }

    public int Coin
    {
        get
        {
            return coin;
        }

        set
        {
            coin = value;
        }
    }

    public int Diamond
    {
        get
        {
            return diamond;
        }

        set
        {
            diamond = value;
        }
    }

    public string NpcTalk
    {
        get
        {
            return npcTalk;
        }

        set
        {
            npcTalk = value;
        }
    }

    public int NpcId
    {
        get
        {
            return npcId;
        }

        set
        {
            npcId = value;
        }
    }

    public int TranscriptId
    {
        get
        {
            return transcriptId;
        }

        set
        {
            transcriptId = value;
        }
    }

    public TaskProgress TaskProgress
    {
        get
        {
            return taskProgress;
        }

        set
        {
            if(taskProgress!=value)
            {
                taskProgress = value;
                OnTaskChange();
            }
    
        }
    }

    public string Icon
    {
        get
        {
            return icon;
        }

        set
        {
            icon = value;
        }
    }

    public string NpcIcon
    {
        get
        {
            return npcIcon;
        }

        set
        {
            npcIcon = value;
        }
    }
}
