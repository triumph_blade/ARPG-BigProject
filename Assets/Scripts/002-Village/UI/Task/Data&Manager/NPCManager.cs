﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCManager : MonoBehaviour {
    public static NPCManager instance;
    public GameObject[] npcArr;
    public GameObject transcript;
    private Dictionary<int, GameObject> npcDict = new Dictionary<int, GameObject>();
    private void Awake()
    {
        instance = this;
        Init();
    }
    private void Start()
    {
       // Init();
    }


    private void Init()
    {
        foreach(GameObject go in npcArr)
        {
            int id = int.Parse(go.name.Substring(0, 4));
            npcDict.Add(id, go);
        }
    }

    public GameObject GetNpcById(int id)
    {
        GameObject go = null;
        npcDict.TryGetValue(id, out go);
        return go;
    }
}
