﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskCtrl : MonoBehaviour {
    private GameObject taskItem;
    private Transform taskListContent;
    private Animation anim;
    private Button closeBtn;
    private bool isShow = false;
    private Task task;

    private void Awake()
    {
        taskItem = Resources.Load<GameObject>("Tasks/TaskItem");
        taskListContent = transform.Find("TaskList/Content");
        anim = transform.GetComponent<Animation>();
        closeBtn = transform.Find("CloseButton").GetComponent<Button>();
        closeBtn.onClick.AddListener(OnCloseBtnClick);
        gameObject.SetActive(false);
    }
    private void Start()
    {
        InitTaskList();
    }

    /// <summary>
    /// 动态生成任务列表
    /// </summary>
    void InitTaskList()
    {
        ArrayList taskList = TaskManager.instance.TaskList;
        foreach(Task task in taskList)
        {
            GameObject taskGo = GameObject.Instantiate(taskItem, taskListContent);
            TaskItemUI tiUI = taskGo.GetComponent<TaskItemUI>();
            tiUI.SetTask(task);
        }
    }


    public void OnShow()
    {
        if (isShow == false)
        {
            gameObject.SetActive(true);
            transform.SetAsLastSibling();
            anim.Play("Anim-LeftUIShow");
            isShow = true;
        }
    }
    void OnCloseBtnClick()
    {
        if (isShow == true)
            StartCoroutine(PlayCloseAnim());
    }

    IEnumerator PlayCloseAnim()
    {
        anim.Play("Anim-LeftUIClose");
        yield return new WaitForSeconds(anim.GetClip("Anim-LeftUIClose").length);
        isShow = false;
        gameObject.SetActive(false);
    }

 

}
