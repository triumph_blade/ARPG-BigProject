﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCDialogCtrl : MonoBehaviour {
    private Animation anim;
    private bool isShow = false;
    private Text npcTalkText;
    private Button acceptBtn;
    private Image npcIcon;

    // Use this for initialization
    void Awake () {
        anim = GetComponent<Animation>();
        npcTalkText = transform.Find("NpcTalk").GetComponent<Text>();
        acceptBtn = transform.Find("AcceptButton").GetComponent<Button>();
        acceptBtn.onClick.AddListener(OnAcceptBtnClick);
        npcIcon = transform.Find("NpcIcon").GetComponent<Image>();
        gameObject.SetActive(false);
	}
	
	public void Show(Task task)
    {
        npcTalkText.text = task.NpcTalk;
        npcIcon.sprite = Resources.Load<Transform>("Tasks/" + task.NpcIcon).GetComponent<SpriteRenderer>().sprite;
        OnShow();
    }


    void OnAcceptBtnClick()
    {
        TaskManager.instance.OnAcceptTask();
        OnClose();
    }

     void OnShow()
    {
        if (isShow == false)
        {
            gameObject.SetActive(true);
            anim.Play("Anim-UIShow");
            isShow = true;
        }
    }



     void OnClose()
    {
        if (isShow == true)
            StartCoroutine(PlayCloseAnim());
    }

    IEnumerator PlayCloseAnim()
    {
        anim.Play("Anim-UIClose");
        float animTime = anim.GetClip("Anim-UIClose").length - 0.2f;
        yield return new WaitForSeconds(animTime);
        isShow = false;
        gameObject.SetActive(false);
    }
}
