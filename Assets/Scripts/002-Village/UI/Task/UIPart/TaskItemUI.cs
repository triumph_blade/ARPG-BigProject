﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskItemUI : MonoBehaviour {

    private Image taskTypeImage;
    private Image iconImage;
    private Text nameText;
    private Text desText;
    private Image reward1Image;
    private Text reward1Text;
    private Image reward2Image;
    private Text reward2Text;
    private Button rewardBtn;
    private Button combatBtn;
    private Text combatBtnText;
    private TaskManager manager;


    private Task task;

    private void Awake()
    {
        taskTypeImage = transform.Find("Type").GetComponent<Image>();
        iconImage = transform.Find("IconBg/Icon").GetComponent<Image>();
        nameText = transform.Find("Name").GetComponent<Text>();
        desText  = transform.Find("Des").GetComponent<Text>();
        reward1Image = transform.Find("Reward1Image").GetComponent<Image>();
        reward1Text = transform.Find("Reward1Text").GetComponent<Text>();
        reward2Image= transform.Find("Reward2Image").GetComponent<Image>();
        reward2Text = transform.Find("Reward2Text").GetComponent<Text>();
        rewardBtn = transform.Find("RewardButton").GetComponent<Button>();
        combatBtn = transform.Find("CombatButton").GetComponent<Button>();
        rewardBtn.onClick.AddListener(OnRewardBtnClick);
        combatBtn.onClick.AddListener(OnCombatBtnClick);
        combatBtnText = transform.Find("CombatButton/Text").GetComponent<Text>();
        manager = TaskManager.instance;
    }




    /// <summary>
    /// 显示任务信息
    /// </summary>
    /// <param name="task"></param>
    public void SetTask(Task task)
    {
        this.task = task;
        task.OnTaskChange += this.OnTaskChange;

        UpdateShow();
    }
    
    void UpdateShow()
    {
        switch (task.TaskType)
        {
            case TaskType.Main:
                taskTypeImage.sprite = Resources.Load<Transform>("Tasks/pic_主线").GetComponent<SpriteRenderer>().sprite;
                break;
            case TaskType.Reward:
                taskTypeImage.sprite = Resources.Load<Transform>("Tasks/pic_奖赏").GetComponent<SpriteRenderer>().sprite;
                break;
            case TaskType.Daily:
                taskTypeImage.sprite = Resources.Load<Transform>("Tasks/pic_日常").GetComponent<SpriteRenderer>().sprite;
                break;
        }
        iconImage.sprite = Resources.Load<Transform>("Tasks/" + task.Icon).GetComponent<SpriteRenderer>().sprite;
        nameText.text = task.Name;
        desText.text = task.Des;
        if (task.Coin > 0 && task.Diamond > 0)
        {
            reward1Image.sprite = Resources.Load<Transform>("Tasks/金币").GetComponent<SpriteRenderer>().sprite;
            reward1Text.text = "X " + task.Coin;
            reward2Image.sprite = Resources.Load<Transform>("Tasks/钻石").GetComponent<SpriteRenderer>().sprite;
            reward2Text.text = "X " + task.Diamond;
        }
        else if (task.Coin > 0)
        {
            reward1Image.sprite = Resources.Load<Transform>("Tasks/金币").GetComponent<SpriteRenderer>().sprite;
            reward1Text.text = "X " + task.Coin;
            reward2Image.gameObject.SetActive(false);
            reward2Text.gameObject.SetActive(false);
        }
        else if (task.Diamond > 0)
        {
            reward1Image.sprite = Resources.Load<Transform>("Tasks/钻石").GetComponent<SpriteRenderer>().sprite;
            reward1Text.text = "X " + task.Diamond;
            reward2Image.gameObject.SetActive(false);
            reward2Text.gameObject.SetActive(false);
        }
        switch (task.TaskProgress)
        {
            case TaskProgress.Unstart:
                rewardBtn.gameObject.SetActive(false);
                combatBtnText.text = "下一步";
                break;
            case TaskProgress.Accept:
                rewardBtn.gameObject.SetActive(false);
                combatBtnText.text = "战斗";
                break;
            case TaskProgress.Complete:
                combatBtn.gameObject.SetActive(false);
                break;
        }
    }
 
    void OnCombatBtnClick()
    {
        manager.OnExcuteTask(task);
        transform.parent.parent.parent.SendMessage("OnCloseBtnClick");
    }


    void OnRewardBtnClick()
    {

    }

    void OnTaskChange()
    {
        UpdateShow();
    }

}
