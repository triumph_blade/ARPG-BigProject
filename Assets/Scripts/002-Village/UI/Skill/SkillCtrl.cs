﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillCtrl : MonoBehaviour {
    private Text skillName;
    private Text skillDes;
    private Button closeBtn;
    private Button upgradeBtn;
    private Text upgradeBtnText;
    private Skill skill;
    private PlayerInfo info;
    private bool isShow = false;
    private Animation anim;

    void Awake()
    {
        skillName = transform.Find("Name").GetComponent<Text>();
        skillDes = transform.Find("Des").GetComponent<Text>();
        closeBtn = transform.Find("CloseButton").GetComponent<Button>();
        closeBtn.onClick.AddListener(() =>{
            if (isShow == true)
            {
                skill = null;
                StartCoroutine(PlayCloseAnim());
            } 
        });
        upgradeBtn = transform.Find("UpgradeButton").GetComponent<Button>();
        anim = transform.GetComponent<Animation>();
        upgradeBtn.onClick.AddListener(()=> {
            int cost = 500 * (skill.Level + 1);
            if (skill.Level < info.Level)
            {
                bool isSuccess = info.Pay(cost);
                if (isSuccess)
                {
                    skill.Upgrade();
                    OnSkillClick(skill);
                }
                else
                    DisableUpgradeBtn("金币不足");
            }
            
        });
        upgradeBtnText = transform.Find("UpgradeButton/Text").GetComponent<Text>();
        skillName.text = "";
        skillDes.text = "";
        DisableUpgradeBtn("选择技能");
        info = PlayerInfo._instance;
        gameObject.SetActive(false);
    }


    void DisableUpgradeBtn(string text="")
    {
        upgradeBtn.interactable = false;
        if (text != "")
            upgradeBtnText.text = text;
    }

    void EnableUpgradeBtn(string text="")
    {
        upgradeBtn.interactable = true;
        if(text!="")
            upgradeBtnText.text = text;
    }

    void OnSkillClick(Skill skill)
    {
        this.skill = skill;
        if ((500 * (skill.Level + 1)) <= info.Coin)
        {
            if (skill.Level < info.Level)
                EnableUpgradeBtn("升级");
            else
                DisableUpgradeBtn("等级限制");
        }
        else
            DisableUpgradeBtn("金币不足");
        skillName.text = skill.Name+" Lv."+skill.Level;
        skillName.color = skill.NameColor;
        skillDes.text = "当前技能的攻击力为：" + skill.Damage * skill.Level + "\n下一级攻击力：" + skill.Damage * (skill.Level + 1) + "  升下一级所需金币：" + 500 * (skill.Level + 1);
    }


    public void OnShow()
    {
        if (isShow == false)
        {
            gameObject.SetActive(true);
            transform.SetAsLastSibling();
            anim.Play("Anim-LeftUIShow");
            isShow = true;
        }
    }


    IEnumerator PlayCloseAnim()
    {
        anim.Play("Anim-LeftUIClose");
        yield return new WaitForSeconds(anim.GetClip("Anim-LeftUIClose").length);
        isShow = false;
        gameObject.SetActive(false);
    }

}
