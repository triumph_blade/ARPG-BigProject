﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SkillItemUI : MonoBehaviour,IPointerClickHandler {
    public PosType posType;
    private Skill skill;
    private Image skillImage;
    private Image SkillImage
    {
        get
        {
            if (skillImage == null)
                skillImage = this.GetComponent<Image>();
            return skillImage;
        }
    }


    void Awake()
    {
        PlayerInfo._instance.OnPlayerInfoChanged += this.OnPlayerInfoChanged;
    }

    void OnPlayerInfoChanged(InfoType type)
    {
        if(type == InfoType.All||type==InfoType.Skill)
        UpdateShow();
    }

    void UpdateShow()
    {
        skill = SkillManager.instance.GetSkillByPostion(posType);
        SkillImage.sprite = Resources.Load<Transform>("Skills/"+skill.Icon).GetComponent<SpriteRenderer>().sprite;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        transform.parent.SendMessage("OnSkillClick", skill);
    }
}
