﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillManager : MonoBehaviour {
    public static SkillManager instance;
    private TextAsset skillInfo;
    private ArrayList skillList = new ArrayList();
    private Dictionary<int, Skill> skillDict = new Dictionary<int, Skill>();

    private void Awake()
    {
        instance = this;
        skillInfo=Resources.Load<TextAsset>("InfoText/SkillListInfo");
        InitSkill();
    }
    void InitSkill()
    {
        string[] skillArray = skillInfo.ToString().Split('\n');
        foreach (string str in skillArray)
        {
            string[] proArray = str.Split(',');
            Skill skill = new Skill();
            skill.Id = int.Parse(proArray[0]);
            skill.Name = proArray[1];
            skill.Icon = proArray[2];
            switch (proArray[3])
            {
                case "Warrior":
                    skill.PlayerType = PlayerType.Warrior;
                    skill.NameColor = new Color(0.28f, 0.81f, 0.9f);
                    break;
                case "Assassin":
                    skill.PlayerType = PlayerType.Assassin;
                    skill.NameColor = new Color(1f, 0.35f, 0.21f);
                    break;
            }
            switch (proArray[4])
            {
                case "Basic":
                    skill.SkillType = SkillType.Basic;
                    break;
                case "Skill":
                    skill.SkillType = SkillType.Skill;
                    break;
            }
            switch (proArray[5])
            {
                case "Basic":
                    skill.PosType = PosType.Basic;
                    break;
                case "One":
                    skill.PosType = PosType.One;
                    break;
                case "Two":
                    skill.PosType = PosType.Two;
                    break;
                case "Three":
                    skill.PosType = PosType.Three;
                    break;
            }
            skill.ColdTime = int.Parse(proArray[6]);
            skill.Damage = int.Parse(proArray[7]);
            skill.Level = 1;
            skillList.Add(skill);
            skillDict.Add(skill.Id, skill);
        }
    }

    public Skill GetSkillByPostion(PosType posType)
    {
        PlayerInfo info = PlayerInfo._instance;
        foreach(Skill skill in skillList)
        {
            if (skill.PlayerType == info.PlayerType && skill.PosType == posType)
                return skill;
        }
        return null;
    }



}
