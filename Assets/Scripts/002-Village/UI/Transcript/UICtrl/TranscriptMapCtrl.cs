﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TranscriptMapCtrl : MonoBehaviour {

    private Button closeBtn;
    private bool isShow = false;
    private Animation anim;
    private TranscriptEnterCtrl transcriptEnterCtrl;

    private void Awake()
    {
        anim = transform.GetComponent<Animation>();
        closeBtn = transform.Find("BackButton").GetComponent<Button>();
        closeBtn.onClick.AddListener(() => {
            if (isShow == true)
            {
                StartCoroutine(PlayCloseAnim());
            }
        });
        transcriptEnterCtrl = transform.Find("TranscriptEnter").GetComponent<TranscriptEnterCtrl>();
        gameObject.SetActive(false);
    }



    public void OnShow()
    {
        if (isShow == false)
        {
            gameObject.SetActive(true);
            transform.SetAsLastSibling();
            anim.Play("Anim-UpUIShow");
            isShow = true;
        }
    }


    IEnumerator PlayCloseAnim()
    {
        anim.Play("Anim-UpUIClose");
        yield return new WaitForSeconds(anim.GetClip("Anim-UpUIClose").length);
        isShow = false;
        gameObject.SetActive(false);
    }

    void OnTranscriptItemClick(TranscriptItemUI transItUI)
    {
        PlayerInfo info = PlayerInfo._instance;
        if(info.Level >= transItUI.needLevel)
        {
            transcriptEnterCtrl.OnLevelEnough(transItUI);
        }
        else
        {
            transcriptEnterCtrl.OnLevelNotEnough();
        }
    }

}
