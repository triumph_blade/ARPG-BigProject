﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TranscriptEnterCtrl : MonoBehaviour {

    private Text desText;
    private Text energyText;
    private Text energyTitleText;
    private Button enterBtn;
    private Button closeBtn;
    private bool isShow = false;
    private Animation anim;

    private void Awake()
    {
        desText = transform.Find("Des").GetComponent<Text>();
        energyText = transform.Find("EnergyTitle/Energy").GetComponent<Text>();
        energyTitleText = transform.Find("EnergyTitle").GetComponent<Text>();
        anim = transform.GetComponent<Animation>();
        closeBtn = transform.Find("CloseButton").GetComponent<Button>();
        closeBtn.onClick.AddListener(() => {
            if (isShow == true)
            {
                StartCoroutine(PlayCloseAnim());
            }
        });
        enterBtn = transform.Find("EnterButton").GetComponent<Button>();
        enterBtn.onClick.AddListener(() =>{
        });
        gameObject.SetActive(false);
    }


    public void OnLevelNotEnough()
    {
        enterBtn.enabled = false;
        energyText.enabled = false;
        energyTitleText.enabled = false;
        desText.text = "当前等级不足，无法进入该副本";
        OnShow();
    }

    public void OnLevelEnough(TranscriptItemUI transItUI)
    {
        energyText.enabled = true;
        energyTitleText.enabled = true;
        enterBtn.enabled = true;
        desText.text = transItUI.des;
        energyText.text = 3.ToString();
        OnShow();
    }
    

     void OnShow()
    {
        if (isShow == false)
        {
            gameObject.SetActive(true);
            transform.SetAsLastSibling();
            anim.Play("Anim-UIShow");
            isShow = true;
        }
    }

    IEnumerator PlayCloseAnim()
    {
        anim.Play("Anim-UIClose");
        yield return new WaitForSeconds(anim.GetClip("Anim-UIClose").length);
        isShow = false;
        gameObject.SetActive(false);
    }
}
