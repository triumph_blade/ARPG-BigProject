﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TranscriptItemUI : MonoBehaviour,IPointerClickHandler {

    public int id;
    public int needLevel;
    public string sceneName;
    public string des = "真正的勇士敢于直面惨淡的人生,敢于正视淋漓的鲜血。勇士无惧，一去兮不复回。";


    public void OnPointerClick(PointerEventData eventData)
    {
        transform.parent.SendMessage("OnTranscriptItemClick",this);
    }

 
}
