﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerBarCtrl : MonoBehaviour {
    //public Sprite[] headSprites;

    private Text nameText;
    private Text levelText;
    private Slider enegySlider;
    private Text enegyText;
    private Slider toughenSlider;
    private Text toughenText;
    private Button addEnegyBtn;
    private Button addToughenBtn;
    private Image headImage;
    private Button headBtn;
    private PlayerInfo info;

    private void Awake()
    {
        info = PlayerInfo._instance;
      ///  headPos = transform.Find("HeadPos");
        nameText = transform.Find("NameText").GetComponent<Text>();
        levelText = transform.Find("LevelText").GetComponent<Text>();
        enegySlider = transform.Find("EnegySlider").GetComponent<Slider>();
        enegyText = transform.Find("EnegySlider/EnegyText").GetComponent<Text>();
        toughenSlider = transform.Find("ToughenSlider").GetComponent<Slider>();
        toughenText = transform.Find("ToughenSlider/ToughenText").GetComponent<Text>();
        addEnegyBtn = transform.Find("AddEnegyButton").GetComponent<Button>();
        addToughenBtn = transform.Find("AddToughenButton").GetComponent<Button>();
        headImage = transform.Find("HeadButton").GetComponent<Image>();
        headBtn = transform.Find("HeadButton").GetComponent<Button>();
        headBtn.onClick.AddListener(OnHeadBtnClick);
        info.OnPlayerInfoChanged += this.OnPlayerInforChanged;
    }

    private void OnDestroy()
    {
        info.OnPlayerInfoChanged-= this.OnPlayerInforChanged;
    }

    void OnPlayerInforChanged(InfoType type)
    {
        if (type == InfoType.All || type == InfoType.HeadPortrait)
            headImage.sprite = Resources.Load<Transform>("UITex/" + info.HeadPortrait).GetComponent<SpriteRenderer>().sprite;
        if (type == InfoType.All || type == InfoType.Level)
            levelText.text = info.Level.ToString();
        if (type == InfoType.All || type == InfoType.Name)
            nameText.text = info.Name;
        if (type == InfoType.All || type == InfoType.Energy)
        {
            enegySlider.value = info.Energy / 100f;
            enegyText.text = info.Energy.ToString() + "/100";
        }
        if (type == InfoType.All || type == InfoType.Toughen)
        {
            toughenSlider.value = info.Toughen / 50f;
            toughenText.text = info.Toughen.ToString() + "/50";
        }
    }
    

    
    void OnHeadBtnClick()
    {
        transform.parent.SendMessage("OnHeadBtnClick");
    }
}
