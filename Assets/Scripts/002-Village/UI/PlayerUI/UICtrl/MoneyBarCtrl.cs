﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyBarCtrl : MonoBehaviour {

    private Text coinText;
    private Text diamondText;
    private Button addCoinBtn;
    private Button addDiamondBtn;

    private PlayerInfo info;

    private void Awake()
    {
        addCoinBtn = transform.Find("CoinBar/AddCoinButton").GetComponent<Button>();
        addDiamondBtn = transform.Find("DiamondBar/AddDiamondButton").GetComponent<Button>();
        coinText = transform.Find("CoinBar/CoinText").GetComponent<Text>();
        diamondText = transform.Find("DiamondBar/DiamondText").GetComponent<Text>();
        info = PlayerInfo._instance;
        info.OnPlayerInfoChanged += this.OnPlayerInfoChange;
    }
    private void OnDestroy()
    {
        info.OnPlayerInfoChanged -= this.OnPlayerInfoChange;
    }

    void OnPlayerInfoChange(InfoType type)
    {
        if (type == InfoType.All || type == InfoType.Coin)
            coinText.text = info.Coin.ToString();
        if (type == InfoType.All || type == InfoType.Diamond)
            diamondText.text = info.Diamond.ToString();
    }

 
}
