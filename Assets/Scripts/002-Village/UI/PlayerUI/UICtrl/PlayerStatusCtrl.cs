﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatusCtrl : MonoBehaviour {
    //public Sprite[] headSprites;

    private Image headImage;
    private Text levelText;
    private Text nameText;
    private Text powerText;
    private Slider expSlider;
    private Text expText;
    private Text diamondText;
    private Text coinText;
    private Text energyText;
    private Text energyPartTimeText;
    private Text energyFullTimeText;
    private Text toughenText;
    private Text toughenPartTimeText;
    private Text toughenFullTimeText;

    private Button closeBtn;
    private Animation anim;
    private  bool isShow = false;

    private Button renameBtn;
    private GameObject renamePanel;
    private InputField renameInput;
    private Button yesBtn;
    private Button noBtn;

    private PlayerInfo info;


    private void Awake()
    {
        headImage = transform.Find("CharacterStatus/HeadImage").GetComponent<Image>();
        levelText = transform.Find("CharacterStatus/LevelText").GetComponent<Text>();
        powerText = transform.Find("CharacterStatus/PowerText").GetComponent<Text>();
        nameText = transform.Find("CharacterStatus/NameText").GetComponent<Text>();
        expSlider = transform.Find("CharacterStatus/ExpSlider").GetComponent<Slider>();
        expText = transform.Find("CharacterStatus/ExpSlider/ExpText").GetComponent<Text>();
        diamondText = transform.Find("MoneyStatus/DiamondStatus/DiamondText").GetComponent<Text>();
        coinText = transform.Find("MoneyStatus/CoinStatus/CoinText").GetComponent<Text>();
        energyText = transform.Find("EnergyStatus/EnergyText").GetComponent<Text>();
        energyPartTimeText  = transform.Find("EnergyStatus/EnergyPartTimeText").GetComponent<Text>();
        energyFullTimeText  = transform.Find("EnergyStatus/EnergyFullTimeText").GetComponent<Text>();
        toughenText = transform.Find("ToughenStatus/ToughenText").GetComponent<Text>();
        toughenPartTimeText = transform.Find("ToughenStatus/ToughenPartTimeText").GetComponent<Text>();
        toughenFullTimeText = transform.Find("ToughenStatus/ToughenFullTimeText").GetComponent<Text>();
        anim = transform.GetComponent<Animation>();
        //按钮关闭面板
        closeBtn = transform.Find("CloseButton").GetComponent<Button>();
        renameBtn = transform.Find("CharacterStatus/RenameButton").GetComponent<Button>();
        renamePanel = transform.Find("RenamePanel").gameObject;
        renameInput = transform.Find("RenamePanel/RenameInputField").GetComponent<InputField>();
        yesBtn = transform.Find("RenamePanel/YesButton").GetComponent<Button>();
        noBtn = transform.Find("RenamePanel/NoButton").GetComponent<Button>();

        closeBtn.onClick.AddListener(OnCloseBtnClick);
        renameBtn.onClick.AddListener(OnRenameBtnClick);
        yesBtn.onClick.AddListener(OnYesBtnClick);
        noBtn.onClick.AddListener(OnNoBtnClick);
        renamePanel.SetActive(false);

        info = PlayerInfo._instance;
        info.OnPlayerInfoChanged += this.OnPlayerInfoChanged;

        gameObject.SetActive(false);
    }

    private void Update()
    {
        UpdateEnergyTimerUIShow();
        UpdateToughenTimerUIShow();
    }

    private void OnDestroy()
    {
        info.OnPlayerInfoChanged -= this.OnPlayerInfoChanged;
    }

    void OnPlayerInfoChanged(InfoType type)
    {
        if (type == InfoType.All || type == InfoType.HeadPortrait)
            headImage.sprite = Resources.Load<Transform>("UITex/" + info.HeadPortrait).GetComponent<SpriteRenderer>().sprite;
        //foreach(Sprite sp in headSprites)
        //    if(info.HeadPortrait == sp.name)
        //    {
        //        headImage.sprite = sp;
        //        break;
        //    }
        if (type == InfoType.All || type == InfoType.Level)
            levelText.text = info.Level.ToString();
        if (type == InfoType.All || type == InfoType.Power)
            powerText.text = info.Power.ToString();
        if (type == InfoType.All || type == InfoType.Name)
            nameText.text = info.Name;
        if (type == InfoType.All || type == InfoType.Exp)
        {
            expSlider.value = (float)info.Exp / info.MaxExp;
            expText.text = info.Exp + "/" + info.MaxExp;
        }
        if (type == InfoType.All || type == InfoType.Diamond)
            diamondText.text = info.Diamond.ToString();
        if (type == InfoType.All || type == InfoType.Coin)
            coinText.text = info.Coin.ToString();
        if (type == InfoType.All || type == InfoType.Energy)
        {
            energyText.text = info.Energy.ToString() + "/100";
            UpdateEnergyTimerUIShow();
        }
        if (type == InfoType.All || type == InfoType.Energy)
        {
            toughenText.text = info.Toughen + "/50";
            UpdateToughenTimerUIShow();
        }   
    }

    void UpdateEnergyTimerUIShow()
    {
        PlayerInfo info = PlayerInfo._instance;
        if (info.Energy >= 100)
        {
            energyFullTimeText.text = "00:00:00";
            energyPartTimeText.text = "00:00:00";
        }
        else
        {
            int seconds = 60 - (int)info.EnergyTimer;
            string secondsStr = seconds <= 9 ? "0" + seconds : seconds.ToString();
            energyPartTimeText.text = "00:00:" + secondsStr;
            //总的体力值为100，因为是一分钟恢复一点体力，最后一点体力的恢复在分钟位显示
            int minutes = 99 - info.Energy;
            int hours = minutes / 60;
            minutes = minutes % 60;
            string hoursStr = hours <= 9 ? "0" + hours : hours.ToString();
            string minutesStr = minutes <= 9 ? "0" + minutes : minutes.ToString();
            energyFullTimeText.text = hoursStr + ":" + minutesStr + ":" + secondsStr;
        }
    }
    
    void UpdateToughenTimerUIShow()
    {
        if (info.Toughen >= 50)
        {
            toughenFullTimeText.text = "00:00:00";
            toughenPartTimeText.text = "00:00:00";
        }
        else
        {
            int seconds = 60 - (int)info.ToughenTimer;
            string secondsStr = seconds <= 9 ? "0" + seconds : seconds.ToString();
            toughenPartTimeText.text = "00:00:" + secondsStr;
            //总的历练值为50，因为是一分钟恢复一点历练，最后一点历练的恢复在分钟位显示
            int minutes = 49 - info.Toughen;
            int hours = minutes / 60;
            minutes = minutes % 60;
            string hoursStr = hours <= 9 ? "0" + hours : hours.ToString();
            string minutesStr = minutes <= 9 ? "0" + minutes : minutes.ToString();   
            toughenFullTimeText.text = hoursStr + ":" + minutesStr + ":" + secondsStr;
        }
    }

    public void OnShow()
    {
        if (isShow == false)
        {
            gameObject.SetActive(true);
            transform.SetAsLastSibling();
            anim.Play("Anim-UpUIShow");
            isShow = true;
        }
    }
    void OnCloseBtnClick()
    {
        if(isShow==true)
            StartCoroutine(PlayCloseAnim());
    }

    IEnumerator PlayCloseAnim()
    {
        anim.Play("Anim-UpUIClose");
        yield return new WaitForSeconds(anim.GetClip("Anim-UpUIClose").length);
        isShow = false;
        gameObject.SetActive(false);
    }

     

    void OnRenameBtnClick()
    {
        renamePanel.SetActive(true);
    }
    
    void OnYesBtnClick()
    {
        //首先校验名称是否重复
        //TODO
        PlayerInfo._instance.Rename(renameInput.text);
        renamePanel.SetActive(false);
    }

    void OnNoBtnClick()
    {
        renamePanel.SetActive(false);
    }
}
