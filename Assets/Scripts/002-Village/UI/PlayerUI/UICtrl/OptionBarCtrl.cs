﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionBarCtrl : MonoBehaviour {

    private Button bagBtn;
    private Button taskBtn;
    private Button skillBtn;
    private Button battleBtn;
    private void Awake()
    {
        bagBtn = transform.Find("BagButton").GetComponent<Button>();
        bagBtn.onClick.AddListener(()=>{
            transform.parent.SendMessage("OnBagBtnClick");
        });
        taskBtn = transform.Find("TaskButton").GetComponent<Button>();
        taskBtn.onClick.AddListener(()=>{
            transform.parent.SendMessage("OnTaskBtnClick");
        });
        skillBtn = transform.Find("SkillButton").GetComponent<Button>();
        skillBtn.onClick.AddListener(() => {
            transform.parent.SendMessage("OnSkillBtnClick");
        });
        battleBtn = transform.Find("BattleButton").GetComponent<Button>();
        battleBtn.onClick.AddListener(() => {
            transform.parent.SendMessage("OnBattleBtnClick");
        });
    }
}
