﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InfoType
{
    Name,
    HeadPortrait,
    Level,
    Power,
    Exp,
    Diamond,
    Coin,
    Energy,
    Toughen,
    Hp,
    Damage,
    Equip,
    Skill,
    All
}

public enum PlayerType
{
    Warrior,
    Assassin
}

public class PlayerInfo : MonoBehaviour { 
    public static PlayerInfo _instance;

    #region property
    private string _name;
    private string _headPortrait;
    private int _level=1;
    private int _power = 1;
    private int _exp = 0;
    private int _maxExp = 0;
    private int _diamond;
    private int _coin;
    private int _energy;
    private int _toughen;
    private float _energyTimer = 0;
    private float _toughenTimer = 0;
    private int _hp;
    private int _damage;
    private PlayerType _playerType;

    private InventoryItem _helmIt;
     private InventoryItem _clothIt;
    private InventoryItem _weaponIt;
    private InventoryItem _necklaceIt;
    private InventoryItem _shoesIt;
    private InventoryItem _braceletIt;
    private InventoryItem _ringIt;
    private InventoryItem _wingIt;

    #endregion

    #region attribute
    public string Name  {  get {  return _name; }   set  {   _name = value; }  }
    public string HeadPortrait{  get {  return _headPortrait; }    set { _headPortrait = value; }  }
    public int Level {get { return _level; }  set { _level = value; }   }
    public int Power { get { return _power; } set { _power = value; } }
    public int Exp {  get { return _exp; }  set { _exp = value; }  }
    public int MaxExp { get { return _maxExp; } set { _maxExp = value; } }
    public int Diamond {get { return _diamond; }   set { _diamond = value; }  }
    public int Coin { get { return _coin; } set { _coin = value; } }
    public int Energy { get { return _energy; } set { _energy = value; } }
    public int Toughen { get { return _toughen; } set { _toughen = value; } }
    public float EnergyTimer { get { return _energyTimer; } set { _energyTimer = value; } }
    public float ToughenTimer { get { return _toughenTimer; } set { _toughenTimer = value; } }
    public int Hp { get { return _hp; } set { _hp = value; } }
    public int Damage { get { return _damage; } set { _damage = value; } }
    public PlayerType PlayerType { get { return _playerType; } set { _playerType = value; } }
    public InventoryItem HelmIt { get { return _helmIt; } set { _helmIt = value; } }
    public InventoryItem ClothIt { get { return _clothIt; } set { _clothIt = value; } }
    public InventoryItem WeaponIt { get { return _weaponIt; } set { _weaponIt = value; } }
    public InventoryItem NecklaceIt { get { return _necklaceIt; } set { _necklaceIt = value; } }
    public InventoryItem ShoesIt { get { return _shoesIt; } set { _shoesIt = value; } }
    public InventoryItem BraceletIt { get { return _braceletIt; } set { _braceletIt = value; } }
    public InventoryItem RingIt { get { return _ringIt; } set { _ringIt = value; } }
    public InventoryItem WingIt { get { return _wingIt; } set { _wingIt = value; } }
    #endregion

    public delegate void OnPlayerInfoChangedEvent(InfoType type);
    public event OnPlayerInfoChangedEvent OnPlayerInfoChanged;

    public void Rename(string newName)
    {
        this.Name = newName;
        OnPlayerInfoChanged(InfoType.Name);
    }

    public InventoryItem OnDressOn(InventoryItem it)
    {
        if (it == null) return null;
        it.IsDressed = true;
        InventoryItem itReplaced = null;
        switch (it.Inventory.EquipType)
        {
            case EquipType.Bracelet:
                if ( BraceletIt != null)
                    itReplaced =  BraceletIt;
                 BraceletIt = it;
                break;
            case EquipType.Cloth:
                if ( ClothIt != null)
                    itReplaced =  ClothIt;
                 ClothIt = it;
                break;
            case EquipType.Helm:
                if ( HelmIt != null)
                    itReplaced =  HelmIt;
                 HelmIt = it;
                break;
            case EquipType.Necklace:
                if ( NecklaceIt != null)
                    itReplaced =  NecklaceIt;
                 NecklaceIt = it;
                break;
            case EquipType.Ring:
                if ( RingIt != null)
                    itReplaced =  RingIt;
                 RingIt = it;
                break;
            case EquipType.Shoes:
                if ( ShoesIt != null)
                    itReplaced =  ShoesIt;
                 ShoesIt = it;
                break;
            case EquipType.Weapon:
                if ( WeaponIt != null)
                    itReplaced =  WeaponIt;
                 WeaponIt = it;
                break;
            case EquipType.Wing:
                if ( WingIt != null)
                    itReplaced =  WingIt;
                 WingIt = it;
                break;
        }
        OnPlayerInfoChanged(InfoType.Equip);
        this.Hp += (int)(it.Inventory.Hp * (1f + (it.Level - 1f) / 10f));
        this.Damage += (int)(it.Inventory.Damage * (1f + (it.Level - 1f) / 10f));
        this.Power += (int)(it.Inventory.Power * (1f + (it.Level - 1f) / 10f));
        OnPlayerInfoChanged(InfoType.Damage);
        OnPlayerInfoChanged(InfoType.Hp);
        OnPlayerInfoChanged(InfoType.Power);
        return itReplaced;
    }

    public void OnDressOff(InventoryItem it)
    {
        if (it == null) return;
        switch (it.Inventory.EquipType)
        {
            case EquipType.Bracelet:
                 BraceletIt = null;
                break;
            case EquipType.Cloth:
                 ClothIt = null;
                break;
            case EquipType.Helm:
                 HelmIt = null;
                break;
            case EquipType.Necklace:
                 NecklaceIt = null;
                break;
            case EquipType.Ring:
                 RingIt = null;
                break;
            case EquipType.Shoes:
                 ShoesIt = null;
                break;
            case EquipType.Weapon:
                 WeaponIt = null;
                break;
            case EquipType.Wing:
                 WingIt = null;
                break;
        }
        it.IsDressed = false;
        OnPlayerInfoChanged(InfoType.Equip);
        this.Hp -=(int)( it.Inventory.Hp * (1f + (it.Level - 1f) / 10f));
        this.Damage -= (int)(it.Inventory.Damage * (1f + (it.Level - 1f) / 10f));
        this.Power -= (int)(it.Inventory.Power * (1f + (it.Level - 1f) / 10f));
        OnPlayerInfoChanged(InfoType.Damage);
        OnPlayerInfoChanged(InfoType.Hp);
        OnPlayerInfoChanged(InfoType.Power);
    }

    public void OnUpgrade(InventoryItem it,int deltaHp,int deltaDamage, int deltaPower)
    {
        this.Hp += deltaHp;
        this.Damage += deltaDamage;
        this.Power += deltaPower;
        OnPlayerInfoChanged(InfoType.Damage);
        OnPlayerInfoChanged(InfoType.Hp);
        OnPlayerInfoChanged(InfoType.Power);
    }

    public bool OnUseItem(InventoryItem it ,int count)
    {
        int applyValue = it.Inventory.ApplyValue * count;
        HintManager hintManager = HintManager.instance;
        switch (it.Inventory.InfoType)
        {
            case InfoType.Energy:
                if (Energy >= 100)
                {
                    hintManager.ShowHint("体力值已满，\n无法使用" + it.Inventory.Name);
                    return true;
                }
                else
                {
                    if (Energy + applyValue > 100)
                    {
                        hintManager.ShowHint("使用了" + it.Inventory.Name + "\n恢复了" +(100-Energy) + "点体力");
                        Energy = 100;
                    } 
                    else if (Energy + applyValue <= 100)
                    {
                        hintManager.ShowHint("使用了" + it.Inventory.Name + "\n恢复了" + applyValue+ "点体力");
                        Energy += applyValue;
                    }
                    OnPlayerInfoChanged(InfoType.Energy);
                    return false;
                }
            case InfoType.Toughen:
                if (Toughen >= 50)
                {
                    hintManager.ShowHint("历练值已满，\n无法使用" + it.Inventory.Name);
                    return true;
                }
                else
                {
                    if (Toughen + applyValue > 50)
                    {
                        hintManager.ShowHint("使用了" + it.Inventory.Name + "\n恢复了" + (50 - Toughen) + "点历练");
                        Toughen = 50;
                    }    
                    else if (Toughen + applyValue <= 50)
                    {
                        hintManager.ShowHint("使用了" + it.Inventory.Name + "\n恢复了" +applyValue + "点历练");
                        Toughen += applyValue;
                    }
                    OnPlayerInfoChanged(InfoType.Toughen);
                    return false;
                }
            case InfoType.Coin:
                hintManager.ShowHint("使用了" + it.Inventory.Name + "\n获得了" + applyValue + "枚金币");
                Coin += applyValue;
                OnPlayerInfoChanged(InfoType.Coin);
                return false;
            case InfoType.Diamond:
                hintManager.ShowHint("使用了" + it.Inventory.Name + "\n获得了" + applyValue + "颗钻石");
                Diamond += applyValue;
                OnPlayerInfoChanged(InfoType.Diamond);
                return false;
        }
        return true;
    }

    public bool Pay(int cost)
    {
        if (Coin >= cost) {
            Coin -= cost;
            OnPlayerInfoChanged(InfoType.Coin);
            return true;
        }
        return false;
    }

    public void Earn(int income)
    {
        Coin += income;
        OnPlayerInfoChanged(InfoType.Coin);
    }


    #region unity event
    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        
        Init();
       
    }

    private void Update()
    {
        //实现体力与历练自动增长
        if(this.Energy<100)
        {
            _energyTimer += Time.deltaTime;
            if (_energyTimer > 60)
            {
                Energy += 1;
                _energyTimer -= 60;
                OnPlayerInfoChanged(InfoType.Energy);
            }
        }
        else
        { 
            this._energyTimer = 0;
        }

        if (this.Toughen < 100)
        {
            _toughenTimer += Time.deltaTime;
            if (_toughenTimer > 60)
            {
                Toughen += 1;
                _toughenTimer -= 60;
                OnPlayerInfoChanged(InfoType.Toughen);
            }
        }
        else
        {
            this._toughenTimer = 0;
        }
    }
    #endregion


    private void Init()
    {
        this.Coin = 23333;
        this.Diamond = 1551;
        this.Energy = 64;
        this.Exp = 23;
        this.HeadPortrait = "头像男";
        this.Level = 15;
        this.Name = "小稽崽";
        this.Power = 23;
        this.Toughen = 32;
        this.PlayerType = PlayerType.Warrior;
        InitMaxExp();

        //this.BraceletID = 1009;
        //this.WingID = 1010;
        //this.RingID = 1011;
        //this.CloseID = 1012;
        //this.HelmID = 1013;
        //this.WeaponID = 1014;
        //this.NecklaceID = 1015;
        //this.ShoesID = 1016;
        InitHpDamagePower();

        OnPlayerInfoChanged(InfoType.All);
    }

    void InitMaxExp()
    {
        this.MaxExp = (int)((this.Level) * (100f + 10 * (this.Level - 1f) / 2));
    }

    void InitHpDamagePower()
    {
        this.Hp = this.Level * 100;
        this.Damage = this.Level * 50;
        this.Power = this.Hp + this.Damage;
        //PutonEquip(HelmID);
        //PutonEquip(RingID);
        //PutonEquip(WingID);
        //PutonEquip(CloseID);
        //PutonEquip(WeaponID);
        //PutonEquip(NecklaceID);
        //PutonEquip(BraceletID);
        //PutonEquip(ShoesID);
    }









  
}
