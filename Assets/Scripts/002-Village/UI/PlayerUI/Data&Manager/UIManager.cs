﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    private PlayerStatusCtrl playerStatusCtrl;
    private KnapsackCtrl knapsackCtrl;
    private TaskCtrl taskCtrl;
    private NPCDialogCtrl npcDialogCtrl;
    private SkillCtrl skillCtrl;
    private TranscriptMapCtrl transcriptMapCtrl;

    private void Awake()
    {
        playerStatusCtrl = transform.Find("PlayerStatus").GetComponent<PlayerStatusCtrl>();
        knapsackCtrl = transform.Find("Knapsack").GetComponent<KnapsackCtrl>();
        taskCtrl = transform.Find("Task").GetComponent<TaskCtrl>();
        npcDialogCtrl = transform.Find("Dialog").GetComponent<NPCDialogCtrl>();
        skillCtrl = transform.Find("Skill").GetComponent<SkillCtrl>();
        transcriptMapCtrl = transform.Find("TranscriptMap").GetComponent<TranscriptMapCtrl>();
    }
     void OnHeadBtnClick()
    {
        playerStatusCtrl.OnShow();
    }
     void OnBagBtnClick()
    {
        knapsackCtrl.OnShow();
    }
     void OnTaskBtnClick()
    {
        taskCtrl.OnShow();
    }
    void OnSkillBtnClick()
    {
        skillCtrl.OnShow();
    }
    void OnBattleBtnClick()
    {
        transcriptMapCtrl.OnShow();
    }


    void OnArriveDestination(object obj)
    {
        Task task = obj as Task;
        //到达npc所在位置
        if(task.TaskProgress == TaskProgress.Unstart)
            npcDialogCtrl.Show(task);
        //TODO:到达副本入口，加载战斗场景   (加载很可能是交给其他类处理)
    }
}
