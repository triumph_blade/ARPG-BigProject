﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SetTextPercent : MonoBehaviour
{
    private Text text;
    private void Awake()
    {
        text = GetComponent<Text>();
    }
    public void SetPercent(Slider slider)
    { 
        text.text =(int)(slider.value*100)+"%";
    }
}
