﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadProgressCtrl : MonoBehaviour {

    //public static LoadProgressCtrl instance;

    private Slider progressBar;
    private bool  isAsyn = false;
    private AsyncOperation ao = null;
    private RectTransform rectTransform;

    private void Awake()
    {
        //instance = this;
        progressBar = transform.Find("ProgressBar").GetComponent<Slider>();
        rectTransform = transform as RectTransform;
        rectTransform.localPosition = Vector3.zero;
        gameObject.SetActive(false);
      
    }

    // Update is called once per frame
    void Update () {
        if (isAsyn)
            progressBar.value = ao.progress;
	}

    public void ShowThis(AsyncOperation ao)
    {
        gameObject.SetActive(true);
        isAsyn = true;
        this.ao = ao;
    }
}
