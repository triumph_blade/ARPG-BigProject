﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintManager : MonoBehaviour {
    public static HintManager instance;
    private Animation anim;
    private Text hintText;
    private Button closeBtn;
    private float animTime;

     void Awake()
    {
        instance = this;
        anim = transform.GetComponent<Animation>();
        hintText = transform.Find("HintText").GetComponent<Text>();
        closeBtn = transform.Find("CloseButton").GetComponent<Button>();
        closeBtn.onClick.AddListener(OnCloseBtnClick);
        transform.localPosition = Vector3.zero;
        gameObject.SetActive(false);
    }

    public void ShowHint(string message)
    {
            gameObject.SetActive(true);
         transform.SetAsLastSibling();
            hintText.text = message;
            StartCoroutine(PlayShowAnim());
    }



    IEnumerator PlayShowAnim()
    {
        anim.Play("Anim-UIShow");
        animTime = anim.GetClip("Anim-UIShow").length;
        yield return new WaitForSeconds(animTime);
        yield return new WaitForSeconds(1f);
        StartCoroutine(PlayCloseAnim());
    }


    void OnCloseBtnClick()
    {
          StartCoroutine(PlayCloseAnim());
    }

    IEnumerator PlayCloseAnim()
    {
        anim.Play("Anim-UIClose");
        animTime = anim.GetClip("Anim-UIClose").length;
        yield return new WaitForSeconds(animTime);
        gameObject.SetActive(false);
    }

}
