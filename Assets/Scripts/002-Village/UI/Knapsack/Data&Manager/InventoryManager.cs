﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public static InventoryManager instance;
    private TextAsset InventoryInfo;
    
    private Dictionary<int, Inventory> inventoryDict = new Dictionary<int, Inventory>();
    private List<InventoryItem> inventoryItemList = new List<InventoryItem>();

    public delegate void OnInventoryChangeEvent();
    public event OnInventoryChangeEvent OnInventoryChange;



    private void Awake()
    {
        instance = this;
        InventoryInfo = Resources.Load<TextAsset>("InfoText/InventoryListInfo");
        ReadInventoryInfo();
    }

    private void Start()
    {
        ReadInventoryItemInfo();
    }

    /// <summary>
    /// 完成所有道具的初始化
    /// </summary>
    void ReadInventoryInfo()
    {
        string str = InventoryInfo.ToString();
        string[] itemStrArr = str.Split("\r\n".ToCharArray());
        foreach (string itemStr in itemStrArr)
        {
            if (itemStr == "") continue;
            string[] proArr = itemStr.Split('|');
            Inventory inventory = new Inventory();
            //ID 名称 图标 类型（Equip，Drug） 装备类型(Helm, Cloth, Weapon, Shoes, Necklace, Bracelet, Ring, Wing) 
            inventory.ID = int.Parse(proArr[0]);
            inventory.Name = proArr[1];
            inventory.Icon = proArr[2];
            inventory.Price = int.Parse(proArr[5]);
            inventory.Des = proArr[13];
            switch (proArr[3])
            {
                case "Equip":
                    inventory.InventoryType = InventoryType.Equip;
                    break;
                case "Drug":
                    inventory.InventoryType = InventoryType.Drug;
                    break;
                case "Box":
                    inventory.InventoryType = InventoryType.Box;
                    break;
            }
            switch (proArr[14])
            {
                case "Blue":
                    inventory.NameColor = new Color(0.31f, 0.71f, 0.78f);
                    break;
                case "Purple":
                    inventory.NameColor = new Color(0.83f, 0.52f, 0.97f);
                    break;
                case "Yellow":
                    inventory.NameColor = new Color(1, 1, 0.72f);
                    break;
            }
            if (inventory.InventoryType == InventoryType.Equip)
            {
                switch (proArr[4])
                {
                    case "Helm":
                        inventory.EquipType = EquipType.Helm;
                        break;
                    case "Cloth":
                        inventory.EquipType = EquipType.Cloth;
                        break;
                    case "Weapon":
                        inventory.EquipType = EquipType.Weapon;
                        break;
                    case "Shoes":
                        inventory.EquipType = EquipType.Shoes;
                        break;
                    case "Necklace":
                        inventory.EquipType = EquipType.Necklace;
                        break;
                    case "Bracelet":
                        inventory.EquipType = EquipType.Bracelet;
                        break;
                    case "Ring":
                        inventory.EquipType = EquipType.Ring;
                        break;
                    case "Wing":
                        inventory.EquipType = EquipType.Wing;
                        break;
                }
                inventory.StarLevel = int.Parse(proArr[6]);
                inventory.Quality = int.Parse(proArr[7]);
                inventory.Damage = int.Parse(proArr[8]);
                inventory.Hp = int.Parse(proArr[9]);
                inventory.Power = int.Parse(proArr[10]);
            }
            //售价 星级 品质 伤害 生命 战斗力 作用类型 作用值 描述
           else
            {
                switch (proArr[11])
                {
                    case "Energy":
                        inventory.InfoType = InfoType.Energy;
                        break;
                    case "Toughen":
                        inventory.InfoType = InfoType.Toughen;
                        break;
                    case "Coin":
                        inventory.InfoType = InfoType.Coin;
                        break;
                    case "Diamond":
                        inventory.InfoType = InfoType.Diamond;
                        break;
                }
                inventory.ApplyValue = int.Parse(proArr[12]);
            }
            inventoryDict.Add(inventory.ID, inventory);
        }
    }

    /// <summary>
    /// 完成角色背包信息的初始化
    /// </summary>
    void ReadInventoryItemInfo()
    {
        //TODO: 需要连接服务器 获取当前角色拥有的物品,20件
        for (int j = 0; j < 20; j++)
        {
            int id = Random.Range(1001, 1021);
            Inventory i = null;
            inventoryDict.TryGetValue(id, out i);
            if (i.InventoryType == InventoryType.Equip)
            {
                InventoryItem it = new InventoryItem();
                it.Inventory = i;
                it.Level = Random.Range(1, 10);
                it.Count = 1;
                inventoryItemList.Add( it);
            }
            else
            {
                //先判断背包里是否存在消耗类道具
                InventoryItem it = null;
                bool isDrugBoxExit = false;
                foreach (InventoryItem temp in inventoryItemList)
                {
                    if (temp.Inventory.ID == id)
                    {
                        isDrugBoxExit = true;
                        it = temp;
                        break;
                    }
                }
                
                if (isDrugBoxExit)
                    it.Count++;
                else
                {
                    it = new InventoryItem();
                    it.Inventory = i;
                    it.Count = 1;
                    inventoryItemList.Add( it);
                }

            }
            OnInventoryChange();
        }
    }


    public int GetItListCount()
    {
        return inventoryItemList.Count;
    }
    public InventoryItem GetItListElement(int index)
    {
        return inventoryItemList[index];
    }
    public void RemoveItListElement(InventoryItem it)
    {
        inventoryItemList.Remove(it);
    }

}
