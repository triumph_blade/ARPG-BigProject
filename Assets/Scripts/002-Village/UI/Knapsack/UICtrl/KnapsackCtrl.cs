﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KnapsackCtrl : MonoBehaviour {

    private EquipDetailCtrl equipDetailCtrl;
    private PropDetailCtrl propDetailCtrl;
    private KsRoleCtrl ksRoleCtrl;
    private KsInventoryCtrl ksInventoryCtrl;
    private Animation anim;
    private Button closeBtn;
    private bool isShow = false;

    private void Awake()
    {
        equipDetailCtrl = transform.Find("EquipDetailPanel").GetComponent<EquipDetailCtrl>();
        propDetailCtrl = transform.Find("PropDetailPanel").GetComponent<PropDetailCtrl>();
        ksRoleCtrl = transform.Find("Role").GetComponent<KsRoleCtrl>();
        ksInventoryCtrl = transform.Find("Inventory").GetComponent<KsInventoryCtrl>();
        anim = transform.GetComponent<Animation>();
        closeBtn = transform.Find("CloseButton").GetComponent<Button>();
        closeBtn.onClick.AddListener(OnCloseBtnClick);

    }

    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void OnInventoryClick(object[] objectArr)
    {
        bool isLeft = (bool)objectArr[0];
        InventoryItem it = null;
        KsInventoryItemUI ksItUI = null;
        KsRoleEquipUI ksREUI = null;
        if (isLeft)
        {
            ksItUI = objectArr[1] as KsInventoryItemUI;
            it = ksItUI.It;
        }
        else
        {
            ksREUI = objectArr[1] as KsRoleEquipUI;
            it = ksREUI.It;
        }
        if (it != null)
        {
            if (it.Inventory.InventoryType == InventoryType.Equip)
            {
                equipDetailCtrl.ShowEquipDetail(ksItUI,ksREUI,isLeft);
                propDetailCtrl.OnCloseBtnClick();
            }
            else
            { 
                propDetailCtrl.ShowPropDetail(ksItUI);
                equipDetailCtrl.OnCloseBtnClick();
            }

            //没有被穿上的装备和其他物品
            if ((it.Inventory.InventoryType == InventoryType.Equip &&it.IsDressed == false) || it.Inventory.InventoryType != InventoryType.Equip)
                ksInventoryCtrl.EnableSale(ksItUI);
        }
        else
        {
            equipDetailCtrl.OnCloseBtnClick();
            propDetailCtrl.OnCloseBtnClick(); 
        }        
    }

    public void OnHpDamagePowerChange(object[] objectArr)
    {
        int[] startValueArr = new int[3];
        int[] endValueArr = new int[3];
        for (int i = 0; i < objectArr.Length / 2; i++)
            startValueArr[i] = (int)objectArr[i];
        for (int i = objectArr.Length / 2; i < objectArr.Length; i++)
            endValueArr[i-objectArr.Length/2] = (int)objectArr[i];
        ksRoleCtrl.PlayValueShowEffect(startValueArr,endValueArr);
    }

    public void OnAddItem(object obj)
    {
        InventoryItem it = obj as InventoryItem;
        ksInventoryCtrl.AddItem(it);
    }

    public void OnReduceItem() {
        ksInventoryCtrl.ReduceItem();
    }
    
    public void OnCloseDetailPanel()
    {
        ksInventoryCtrl.DisableSale();
    }

    public void OnSaleDone()
    {
        equipDetailCtrl.OnCloseBtnClick();
        propDetailCtrl.OnCloseBtnClick();
    }

    public void OnShow()
    {
        if (isShow == false)
        {
            gameObject.SetActive(true);
            transform.SetAsLastSibling();
            anim.Play("Anim-RightUIShow");
            isShow = true;
        }
    }
    void OnCloseBtnClick()
    {
        if (isShow == true)
        {
            equipDetailCtrl.gameObject.SetActive(false);
            equipDetailCtrl.IsShow = false;
            propDetailCtrl.gameObject.SetActive(false);
            propDetailCtrl.IsShow = false;
            StartCoroutine(PlayCloseAnim());
        }

    }

    IEnumerator PlayCloseAnim()
    {
        anim.Play("Anim-RightUIClose");
        yield return new WaitForSeconds(anim.GetClip("Anim-RightUIClose").length);
        isShow = false;
        gameObject.SetActive(false);
    }

}
