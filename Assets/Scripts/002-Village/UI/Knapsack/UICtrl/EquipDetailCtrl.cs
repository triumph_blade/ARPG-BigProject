﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipDetailCtrl : MonoBehaviour {

    public Sprite[] equipSprites;

    private InventoryItem it;
    private KsInventoryItemUI ksItUI;
    private KsRoleEquipUI ksREUI;

    private Image equipImage;
    private Text nameText;
    private Text qualityText;
    private Text damageText;
    private Text hpText;
    private Text levelText;
    private Text powerText;
    private Text desText;
    private Button closeBtn;
    private Button equipBtn;
    private Text equipBtnText;
    private Button upgradeBtn;
    private ValueShowEffect hpVSE;
    private ValueShowEffect damageVSE;
    private ValueShowEffect levelVSE;
    private ValueShowEffect powerVSE;
   /// private KsInventoryCtrl ksInventoryCtrl;
    private Animation anim;
    private Text upgradePriceText;
    private int upgradePrice;
    private bool isShow=false;
    public bool IsShow { set { isShow = value; } }
    private bool isLeft = true;
    private int hp;
    private int damage;
    private int power;

    private PlayerInfo info;

    public void Awake()
    {
        info = PlayerInfo._instance;
        anim = transform.GetComponent<Animation>();
        equipImage = transform.Find("EquipBg/EquipImage").GetComponent<Image>();
        nameText = transform.Find("NameText").GetComponent<Text>();
        qualityText =transform.Find("QualityText").GetComponent<Text>();
        damageText = transform.Find("DamageText").GetComponent<Text>();
        hpText = transform.Find("HpText").GetComponent<Text>();
        levelText = transform.Find("LevelText").GetComponent<Text>();
        powerText = transform.Find("PowerText").GetComponent<Text>();
        desText  = transform.Find("DesText").GetComponent<Text>();
        closeBtn = transform.Find("CloseButton").GetComponent<Button>();
        closeBtn.onClick.AddListener(OnCloseBtnClick);
        equipBtn = transform.Find("EquipButton").GetComponent<Button>();
        equipBtn.onClick.AddListener(OnEquipBtnClick);
        equipBtnText = transform.Find("EquipButton/Text").GetComponent<Text>();
        upgradeBtn = transform.Find("UpgradeButton").GetComponent<Button>();
        upgradePriceText = transform.Find("UpgradeButton/PriceText").GetComponent<Text>();
        upgradeBtn.onClick.AddListener(OnUpgradeBtnClick);
        hpVSE = hpText.transform.GetComponent<ValueShowEffect>();
        damageVSE = damageText.transform.GetComponent<ValueShowEffect>();
        powerVSE = powerText.transform.GetComponent<ValueShowEffect>();
        levelVSE = levelText.transform.GetComponent<ValueShowEffect>();
        //ksInventoryCtrl = transform.parent.Find("Inventory").GetComponent<KsInventoryCtrl>();
        gameObject.SetActive(false);
    }

    public void ShowEquipDetail( KsInventoryItemUI ksItUI, KsRoleEquipUI ksREUI, bool isLeft = true)
    {
        gameObject.SetActive(true);
        OnShow();

        this.isLeft = isLeft;
        Vector3 pos = transform.localPosition;
        if (isLeft)
        {
            transform.localPosition = new Vector3(-Mathf.Abs(pos.x), pos.y, pos.z);
            equipBtnText.text = "装备";
        }
        else
        {
            transform.localPosition = new Vector3(Mathf.Abs(pos.x), pos.y, pos.z);
            equipBtnText.text = "卸下";
        }

        this.ksItUI = ksItUI;
        this.ksREUI = ksREUI;
        if (ksItUI != null)
            this.it = ksItUI.It;
        if (ksREUI != null)
            this.it = ksREUI.It;
        foreach(Sprite sp in equipSprites)
        {
            if(it.Inventory.Icon == sp.name)
            {
                equipImage.sprite = sp;
                break;
            }
        }

        nameText.text = it.Inventory.Name;
        qualityText.text = it.Inventory.Quality.ToString();
        levelText.text = it.Level.ToString();
        desText.text = it.Inventory.Des;
        nameText.color =qualityText.color= it.Inventory.NameColor;

        upgradePrice = (it.Level + 1) * it.Inventory.Price;
        damage = (int)(it.Inventory.Damage * (1f + (it.Level - 1f) / 10f));
        hp = (int)(it.Inventory.Hp * (1f + (it.Level - 1f) / 10f));
        power = (int)(it.Inventory.Power * (1f + (it.Level - 1f) / 10f));
        upgradePriceText.text = "$" + upgradePrice;
        damageText.text = damage.ToString();
        hpText.text = hp.ToString();
        powerText.text = power.ToString();
    }



    public void OnShow()
    {
        if (isShow == false)
        {
            gameObject.SetActive(true);
            anim.Play("Anim-UIShow");
            isShow = true;
        }
    }
   public void OnCloseBtnClick()
    {
        if (isShow == true)
        {
            it = null;
            ksItUI = null;
            ksREUI = null;
            transform.parent.SendMessage("OnCloseDetailPanel");
            StartCoroutine(PlayCloseAnim());
        }

    }

    IEnumerator PlayCloseAnim()
    {
        anim.Play("Anim-UIClose");
        float animTime = anim.GetClip("Anim-UIClose").length - 0.2f;
        yield return new WaitForSeconds(animTime);
        isShow = false;
        gameObject.SetActive(false);
    }




    void OnEquipBtnClick()
    {
        object[] objectArr = new object[6];
        objectArr[0] = info.Hp;
        objectArr[1] = info.Damage;
        objectArr[2] = info.Power;
        if (isLeft)
        {
            ksItUI.ResetItemImage();
            InventoryItem itReplaced = info.OnDressOn(it);
            if (itReplaced != null)
            {
                itReplaced.IsDressed = false;
                object obj = new object();
                obj = itReplaced;
                transform.parent.SendMessage("OnAddItem", obj);
            }
        }
        else
        {
            ksREUI.ResetEquipImage();
            info.OnDressOff(it);
            object obj = new object();
            obj = it;
            transform.parent.SendMessage("OnAddItem", obj);
        }
        objectArr[3] = info.Hp;
        objectArr[4] = info.Damage;
        objectArr[5] = info.Power;
        transform.parent.SendMessage("OnHpDamagePowerChange", objectArr);
        transform.parent.SendMessage("OnReduceItem");
        OnCloseBtnClick();
    }

    void OnUpgradeBtnClick()
    {
        int lastHp=hp;
        int lastDamage = damage;
        int lastPower = power;
        int lastLevel = it.Level;
        if (it.Level >= 15)
        {
            HintManager.instance.ShowHint("装备已达最大等级，\n无法再升级");
        }
        else
        {
            object[] objectArr = new object[6];
            objectArr[0] = info.Hp;
            objectArr[1] = info.Damage;
            objectArr[2] = info.Power;
            bool isSuccess = info.Pay(upgradePrice);
            if (isSuccess)
            {

                //播放升级音效，动效
                Upgrade(it);
                hpVSE.PlayValueShowEffect(lastHp, hp);
                damageVSE.PlayValueShowEffect(lastDamage, damage);
                levelVSE.PlayValueShowEffect(lastLevel, it.Level);
                powerVSE.PlayValueShowEffect(lastPower, power);
                HintManager.instance.ShowHint("升级成功,\n装备等级为" + it.Level);
                upgradePrice = (it.Level + 1) * it.Inventory.Price;
                upgradePriceText.text = "$" + upgradePrice;
                if(it.IsDressed==true)
                  info.OnUpgrade(it,hp-lastHp,damage-lastDamage,power-lastPower);
                objectArr[3] = info.Hp;
                objectArr[4] = info.Damage;
                objectArr[5] = info.Power;
                transform.parent.SendMessage("OnHpDamagePowerChange", objectArr);
            }
            else
            {
                //给出提示信息，金币不足
                HintManager.instance.ShowHint("金币不足，\n无法升级装备");
            }
        }
    }


    void Upgrade(InventoryItem it)
    {
        it.Level += 1;
        levelText.text = it.Level.ToString();
        damage =  (int)(it.Inventory.Damage * (1f+(it.Level-1f)/ 10f));
        damageText.text = damage.ToString();
        hp = (int)(it.Inventory.Hp * (1f + (it.Level - 1f) / 10f));
        hpText.text = hp.ToString();
        power = (int)(it.Inventory.Power * (1f + (it.Level - 1f) / 10f));
        powerText.text = power.ToString();
    }





    
}
