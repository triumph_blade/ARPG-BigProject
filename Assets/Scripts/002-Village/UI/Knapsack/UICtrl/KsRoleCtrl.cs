﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KsRoleCtrl : MonoBehaviour {

    private KsRoleEquipUI helmEquip;
    private KsRoleEquipUI necklaceEquip;
    private KsRoleEquipUI clothEquip;
    private KsRoleEquipUI weaponEquip;
    private KsRoleEquipUI braceletEquip;
    private KsRoleEquipUI ringEquip;
    private KsRoleEquipUI shoesEquip;
    private KsRoleEquipUI wingEquip;
    private Text hpText;
    private Text damageText;
    private Text powerText;
    private Text expText;
    private ValueShowEffect hpVSE;
    private ValueShowEffect powerVSE;
    private ValueShowEffect damageVSE;
    private Slider expSlider;
    //private Button closeButton;
    //private Animation anim;
    //private bool isShow = true;
    private PlayerInfo info;

    private void Awake()
    {
        info = PlayerInfo._instance;
        helmEquip = transform.Find("EquipGrid/HelmImage").GetComponent<KsRoleEquipUI>();
        necklaceEquip = transform.Find("EquipGrid/NecklaceImage").GetComponent<KsRoleEquipUI>();
        clothEquip = transform.Find("EquipGrid/ClothImage").GetComponent<KsRoleEquipUI>();
        weaponEquip = transform.Find("EquipGrid/WeaponImage").GetComponent<KsRoleEquipUI>();
        braceletEquip = transform.Find("EquipGrid/BraceletImage").GetComponent<KsRoleEquipUI>();
        ringEquip = transform.Find("EquipGrid/RingImage").GetComponent<KsRoleEquipUI>();
        shoesEquip = transform.Find("EquipGrid/ShoesImage").GetComponent<KsRoleEquipUI>();
        wingEquip = transform.Find("EquipGrid/WingImage").GetComponent<KsRoleEquipUI>();
        hpText = transform.Find("HpImage/HpText").GetComponent<Text>();
        damageText = transform.Find("DamageImage/DamageText").GetComponent<Text>();
        powerText = transform.Find("PowerImage/PowerText").GetComponent<Text>();
        expText = transform.Find("ExpSlider/ExpText").GetComponent<Text>();
        damageVSE = damageText.transform.GetComponent<ValueShowEffect>();
        hpVSE = hpText.transform.GetComponent<ValueShowEffect>();
        powerVSE = powerText.transform.GetComponent<ValueShowEffect>();
        expSlider = transform.Find("ExpSlider").GetComponent<Slider>();
       /// closeButton = transform.Find("CloseButton").GetComponent<Button>();
      //  closeButton.onClick.AddListener(OnCloseBtnClick);
        //anim = transform.GetComponent<Animation>();
        info.OnPlayerInfoChanged += this.OnPlayerInfoChanged;
    }

    private void OnDestroy()
    {
        info.OnPlayerInfoChanged -= this.OnPlayerInfoChanged;
    }

    void OnPlayerInfoChanged(InfoType type)
    {
        if (type == InfoType.All || type == InfoType.Equip)
        {
            helmEquip.UpdateEquipImage(info.HelmIt);
            clothEquip.UpdateEquipImage(info.ClothIt);
            weaponEquip.UpdateEquipImage(info.WeaponIt);
            shoesEquip.UpdateEquipImage(info.ShoesIt);
            necklaceEquip.UpdateEquipImage(info.NecklaceIt);
            braceletEquip.UpdateEquipImage(info.BraceletIt);
            ringEquip.UpdateEquipImage(info.RingIt);
            wingEquip.UpdateEquipImage(info.WingIt);
        }
        if (type == InfoType.All || type == InfoType.Hp)
            hpText.text = info.Hp.ToString();
        if (type == InfoType.All || type == InfoType.Damage)
            damageText.text = info.Damage.ToString();
        if (type == InfoType.All || type == InfoType.Exp)
        {
            expText.text = info.Exp+"/"+info.MaxExp;
            expSlider.value = (float)info.Exp / info.MaxExp;
        }
        if (type == InfoType.All || type == InfoType.Power)
            powerText.text = info.Power.ToString();
    }

    public void PlayValueShowEffect(int[] startValueArr,int[] endValueArr)
    {
        hpVSE.PlayValueShowEffect(startValueArr[0], endValueArr[0]);
        damageVSE.PlayValueShowEffect(startValueArr[1], endValueArr[1]);
        powerVSE.PlayValueShowEffect(startValueArr[2], endValueArr[2]);
    }

    //public void OnShow()
    //{
    //    if (isShow == false)
    //    {
    //        gameObject.SetActive(true);
    //        anim.Play("Anim-UIShow");
    //        isShow = true;
    //    }
    //}

    //public void OnCloseBtnClick()
    //{
    //    if (isShow == true)
    //    {
    //       // transform.parent.SendMessage("OnCloseDetailPanel");
    //        StartCoroutine(PlayCloseAnim());
    //    }

    //}

    //IEnumerator PlayCloseAnim()
    //{
    //    anim.Play("Anim-UIClose");
    //    float animTime = anim.GetClip("Anim-UIClose").length - 0.2f;
    //    yield return new WaitForSeconds(animTime);
    //    isShow = false;
    //    gameObject.SetActive(false);
    //}

}
