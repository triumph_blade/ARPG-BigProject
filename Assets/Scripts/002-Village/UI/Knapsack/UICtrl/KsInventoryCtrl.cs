﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KsInventoryCtrl : MonoBehaviour {

    private Transform itemImageFather;
    private List<KsInventoryItemUI> ksItList = new List<KsInventoryItemUI>(); //所有物品格
    private Text itCountText;
    private Text priceText;
    private Button clearupBtn;
    private Button saleBtn;
    private int itCount=0;
    private InventoryManager manager;
    private KsInventoryItemUI ksItUI;

    private void Awake()
    {
        manager = InventoryManager.instance;
        itemImageFather = transform.Find("ItemList/Content");
        foreach (Transform child in itemImageFather)
        {
            KsInventoryItemUI ksit = null;
            ksit = child.GetComponentInChildren<KsInventoryItemUI>();
            if (ksit != null)
                ksItList.Add(ksit);
        }
        itCountText = transform.Find("InventoryCount").GetComponent<Text>();
        priceText = transform.Find("PriceImage/PriceText").GetComponent<Text>();
        clearupBtn = transform.Find("ClearupButton").GetComponent<Button>();
        saleBtn = transform.Find("SaleButton").GetComponent<Button>();
        clearupBtn.onClick.AddListener(OnClearupBtnClick);
        saleBtn.onClick.AddListener(OnSaleBtnClick);
        DisableSale();
        manager.OnInventoryChange += this.OnInventoryChange;
    }
    private void OnDestroy()
    {
        manager.OnInventoryChange -= this.OnInventoryChange;
    }

    void OnInventoryChange()
    {
        int temp = 0;
        for(int i = 0; i < manager.GetItListCount(); i++)
        {
            InventoryItem it = manager.GetItListElement(i);
            if(it.IsDressed == false)
            {
                if (i < ksItList.Count)
                {
                    ksItList[temp].UpdateItemImage(it);
                    temp++;
                }
                else
                {
                    //TODO:提示背包满了 
                }
            }
        }
        itCount = temp;
        for (int i =temp; i < ksItList.Count; i++)
                ksItList[i].ResetItemImage();
        itCountText.text =itCount + "/"+ksItList.Count;
    }

    public void AddItem(InventoryItem it)
    {
        foreach(KsInventoryItemUI ksIt in ksItList)
        {
            if (ksIt.It == null ){
                ksIt.UpdateItemImage(it);
                itCount++;
                itCountText.text = itCount + "/" + ksItList.Count;
                break;
            }
        }
    }

    public void ReduceItem()
    {
        itCount = 0;
        foreach (KsInventoryItemUI ksIt in ksItList)
            if (ksIt.It != null)
                itCount++;
        itCountText.text = itCount + "/" + ksItList.Count;
    }



    void OnClearupBtnClick()
    {
        OnInventoryChange();
    }

    void OnSaleBtnClick()
    {
        int price = int.Parse(priceText.text);
        PlayerInfo._instance.Earn(price);
        manager.RemoveItListElement(ksItUI.It);
        ksItUI.ResetItemImage();
        transform.parent.SendMessage("OnSaleDone");
        HintManager.instance.ShowHint("成功出售，\n获得金钱$"+price);
    }

    public void EnableSale(KsInventoryItemUI ksItUI)
    {
        this.ksItUI = ksItUI;
        saleBtn.interactable = true;
        priceText.text = (ksItUI.It.Inventory.Price * ksItUI.It.Count).ToString();

    }
    
    public void DisableSale()
    {
        saleBtn.interactable = false;
        priceText.text = "$0";
    }


}
