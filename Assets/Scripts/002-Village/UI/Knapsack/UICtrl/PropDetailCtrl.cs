﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PropDetailCtrl : MonoBehaviour {
    public Sprite[] propSprites;

    private InventoryItem it;
    private KsInventoryItemUI ksItUI;
    private Image propImage;
    private Text nameText;
    private Text desText;
    private Text useMoreBtnText;
    private Button closeBtn;
    private Button useBtn;
    private Button useMoreBtn;
    private Animation anim;
    private bool isShow = false;
    public bool IsShow { set { isShow = value; } }

    private void Awake()
    {
        anim = transform.GetComponent<Animation>();
        propImage = transform.Find("PropBg/PropImage").GetComponent<Image>();
        nameText = transform.Find("NameText").GetComponent<Text>();
        desText = transform.Find("DesText").GetComponent<Text>();
        useMoreBtnText = transform.Find("UseMoreButton/Text").GetComponent<Text>();
        closeBtn = transform.Find("CloseButton").GetComponent<Button>();
        closeBtn.onClick.AddListener(OnCloseBtnClick);
        useBtn = transform.Find("UseButton").GetComponent<Button>();
        useBtn.onClick.AddListener(OnUseBtnClick);
        useMoreBtn = transform.Find("UseMoreButton").GetComponent<Button>();
        useMoreBtn.onClick.AddListener(OnUseMoreBtnClick);
        gameObject.SetActive(false);
    }

    public void ShowPropDetail(KsInventoryItemUI ksItUI)
    {
        gameObject.SetActive(true);
        OnShow();
        this.ksItUI = ksItUI;
        this.it = ksItUI.It;
        nameText.text = it.Inventory.Name;
        desText.text = it.Inventory.Des;
        foreach (Sprite sp in propSprites)
        {
            if (it.Inventory.Icon == sp.name)
            {
                propImage.sprite = sp;
                break;
            }
        }
        useMoreBtnText.text = "批量使用(" + it.Count + ")";
    }

    public void OnShow()
    {
        if (isShow == false)
        {
            gameObject.SetActive(true);
            anim.Play("Anim-UIShow");
            isShow = true;
        }
    }
    public void OnCloseBtnClick()
    {
        if (isShow == true)
        {
            it = null;
            ksItUI = null;
            transform.parent.SendMessage("OnCloseDetailPanel");
            StartCoroutine(PlayCloseAnim());
        }
            
    }

    IEnumerator PlayCloseAnim()
    {
        anim.Play("Anim-UIClose");
        float animTime = anim.GetClip("Anim-UIClose").length - 0.2f;
        yield return new WaitForSeconds(animTime);
        isShow = false;
        gameObject.SetActive(false);
    }

    void OnUseBtnClick()
    {
        UseItem(1);
    }

    void OnUseMoreBtnClick()
    {
        UseItem(it.Count);
    }

    void UseItem(int count)
    {
        bool isFull= PlayerInfo._instance.OnUseItem(it, count);
        if (isFull)
        {
            OnCloseBtnClick();
            return;
        }
        ksItUI.ChangeCount(count);
        it.Count -= count;
        if (it.Count <= 0)
        {
            InventoryManager.instance.RemoveItListElement(it);
            transform.parent.SendMessage("OnReduceItem");
        }
        OnCloseBtnClick();
    }
 
    

}
