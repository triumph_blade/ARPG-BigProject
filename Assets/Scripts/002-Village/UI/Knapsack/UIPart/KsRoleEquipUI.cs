﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KsRoleEquipUI : MonoBehaviour {

    private Image equipImage;
    private Button equipBtn;
    private Transform label;
    private InventoryItem it;
    public InventoryItem It { get { return it; } }
    private void Start()
    {
        label = transform.Find("Label");
        label.gameObject.SetActive(false);
        equipImage = transform.GetComponent<Image>();
        equipBtn = transform.GetComponent<Button>();
        equipBtn.onClick.AddListener(OnEquipBtnClick);
    }


    public void UpdateEquipImage(InventoryItem it)
    {
        if (it != null)
        {
            this.it = it;
            //foreach (Sprite sp in equipSprites)
            //{
            //    if(it.Inventory.Icon == sp.name)
            //    {
            //        equipImage.sprite = sp;
            //        break;
            //    }
            //}
            equipImage.sprite= Resources.Load<Transform>("Equips/"+it.Inventory.Icon).GetComponent<SpriteRenderer>().sprite;
            label.gameObject.SetActive(true);
        }
    }

    public void ResetEquipImage()
    {
        this.it = null;
        equipImage.sprite = Resources.Load<Transform>("UITex/bg_道具").GetComponent<SpriteRenderer>().sprite;
        label.gameObject.SetActive(false);
    }

    public void OnEquipBtnClick()
    {
        object[] objectArr = new object[2];
        objectArr[0] = false;
        objectArr[1] = this;
        transform.parent.parent.parent.SendMessage("OnInventoryClick", objectArr);
    }
}
