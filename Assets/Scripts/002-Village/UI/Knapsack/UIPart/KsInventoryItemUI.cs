﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KsInventoryItemUI : MonoBehaviour {
    //public Sprite[] itemSprites;

    private Image itemImage;
    private Text itemCount;
  
    private  InventoryItem it;
    public InventoryItem It { get { return it; } }
    private Button itemBtn;
    private void Awake()
    {
        itemImage = transform.GetComponent<Image>();
        itemBtn = transform.GetComponent<Button>();
        itemCount = transform.Find("ItemCount").GetComponent<Text>();
        itemBtn.onClick.AddListener(OnItemBtnClick);
    }
    public void UpdateItemImage(InventoryItem it)
    {
        this.it = it;
        //foreach(Sprite sp in itemSprites)
        //{
        //    if(it.Inventory.Icon == sp.name)
        //    {
        //        itemImage.sprite = sp;
        //        break;
        //    }
        //}
        if(it.Inventory.InventoryType == InventoryType.Equip)
            itemImage.sprite = Resources.Load<Transform>("Equips/" + it.Inventory.Icon).GetComponent<SpriteRenderer>().sprite;
        else
            itemImage.sprite = Resources.Load<Transform>("Props/" + it.Inventory.Icon).GetComponent<SpriteRenderer>().sprite;
        if (it.Count <= 1)
            itemCount.text = "";
        else
            itemCount.text = it.Count.ToString();
    }
    public void ResetItemImage()
    {
        this.it = null;
        itemCount.text = "";
        itemImage.sprite = Resources.Load<Transform>("UITex/bg_道具").GetComponent<SpriteRenderer>().sprite;
    }
    public void OnItemBtnClick()
    {
        object[] objectArr = new object[2];
        objectArr[0] = true;
        objectArr[1] = this;
        transform.parent.parent.parent.parent.SendMessage("OnInventoryClick", objectArr);
    }

    public void ChangeCount(int count)
    {
        if (it.Count -count<= 0)
            ResetItemImage();
        else if(it.Count-count == 1)
            itemCount.text = "";
        else
            itemCount.text = (it.Count-count).ToString();
    }
}
