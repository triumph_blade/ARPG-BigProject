﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValueShowEffect : MonoBehaviour {

    private float startValue=0;
    private int endValue=0;
    private bool isStart = false;
    private bool isUp = true;
    private int speed;

    private Text valueText;

    private void Awake()
    {
        valueText = transform.GetComponent<Text>();
    
    }

    private void Update()
    {
        if (isStart)
        {
            if (isUp)
            {
                startValue += speed * Time.deltaTime;
                if (startValue >=endValue)
                {
                    startValue = endValue;
                    valueText.text = ((int)startValue).ToString();
                    isStart = false;
                }
            }
            else
            {
                startValue -= speed * Time.deltaTime;
                if (startValue <= endValue)
                {
                    startValue = endValue;
                    valueText.text = ((int)startValue).ToString();
                    isStart = false;
                }
            }
            valueText.text = ((int)startValue).ToString();
        }
    }

    public void PlayValueShowEffect(int startValue,int endValue)
    {
        this.startValue = startValue;
        this.endValue = endValue;
        if (endValue > startValue)
        {
            isUp = true;
            speed = endValue - startValue;
        }
        else
        {
            isUp = false;
            speed = startValue - endValue;
        }
        isStart = true;
    }

}
